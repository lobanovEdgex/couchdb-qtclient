#include "testcouchdbattachmenthandler.h"

#include "testutils.h"
#include "utils.h"
#include "src/couchdbproperties.h"
#include "src/couchdbclient.h"
#include "src/couchdbattachmenthandler.h"

#include <QMap>
#include <QVariant>
#include <QVariantMap>
#include <QString>
#include <QByteArray>

using namespace cdbqt;



TestCouchDbAttachmentHandler::TestCouchDbAttachmentHandler(QObject *parent) :
    QObject(parent)
{
}

void TestCouchDbAttachmentHandler::initTestCase()
{
    QString dbName = "couchdb-attachment-handler-test";

    CouchDbProperties *properties = TestUtils::defaultPropertiesForDatabase(dbName);

    TestUtils::deleteDatabase(properties);

    cdbClient = new CouchDbClient(properties, this);

    QSignalSpy spy(cdbClient, SIGNAL(couchDbClientFinishedInitialization(cdbqt::CouchDbError::Error)));
    QVERIFY(spy.isValid());

    cdbClient->init();

    QTRY_COMPARE(spy.count(), 1);
    QVariant variant = qvariant_cast<QVariant>(spy.at(0).first());
    CouchDbError::Error error = variant.value<CouchDbError::Error>();
    QCOMPARE(error, CouchDbError::NO_ERROR);
}

void TestCouchDbAttachmentHandler::cleanupTestCase()
{

}

void TestCouchDbAttachmentHandler::testGetAttachment()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_get_attachment";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QVariantMap document = cdbqt::documentWithId(documentId);

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());

    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));
    QString revision = receiver->document["rev"].toString();

    QString attachmentName = "attachment123";
    QByteArray inputData = QString("abra-cadabra").toLatin1();

    QString pathToAttachment = documentId + "/" + attachmentName;
    QUrl urlToAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    urlToAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));
    spy.clear();
    cdbClient->put(receiver, urlToAttachment, inputData, QVariant(3));
    QTRY_COMPARE(spy.count(), 1);
    qDebug() << receiver->document;
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)3));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("2"));
    QString attachmentRevision = receiver->document["rev"].toString();

    CouchDbAttachmentHandler *handler = cdbClient->createAttachmentHandler(this);
    QVERIFY(handler != NULL);

    QSignalSpy attachmentSpy(handler, SIGNAL(attachmentReady(QString,QString,QByteArray,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(attachmentSpy.isValid());

    handler->getAttachment(documentId, attachmentName,  QVariant(100));
    QTRY_COMPARE(attachmentSpy.count(), 1);
    QCOMPARE(attachmentSpy.at(0).at(0).toString(), documentId);
    QCOMPARE(attachmentSpy.at(0).at(1).toString(), attachmentName);
    QCOMPARE(attachmentSpy.at(0).at(2).toByteArray(), inputData);
    QCOMPARE(TestUtils::getErrorArgument(attachmentSpy.at(0), 3), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(attachmentSpy.at(0), 4), QVariant(100));
}

void TestCouchDbAttachmentHandler::testGetAttachments()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_get_attachments";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QVariantMap document = cdbqt::documentWithId(documentId);

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());

    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));
    QString revision = receiver->document["rev"].toString();
    QList<QString> attachmentList;

    const int indexOfAttachmentToSkip = 3;

    const int attachmentsSize = 5;
    for(int i = 0; i < attachmentsSize; i++) {
        QString attachmentName = QString("attachment%1").arg(i);
        attachmentList.append(attachmentName);
        if(i == indexOfAttachmentToSkip) {
            continue;
        }
        QByteArray inputData = QString("abra-cadabra%1").arg(i).toLatin1();

        QUrl urlToAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
        urlToAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));

        spy.clear();
        cdbClient->put(receiver, urlToAttachment, inputData, QVariant(3));
        QTRY_COMPARE(spy.count(), 1);
        qDebug() << receiver->document;
        QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
        QCOMPARE(receiver->userData, QVariant((int)3));
        QCOMPARE(receiver->document.count(), 3);
        QCOMPARE(receiver->document["id"].toString(), documentId);
        QCOMPARE(receiver->document["ok"].toBool(), true);
        if(i < indexOfAttachmentToSkip) {
            QVERIFY(receiver->document["rev"].toString().startsWith(QString("%1").arg(i + 2))); //
        } else {
            QVERIFY(receiver->document["rev"].toString().startsWith(QString("%1").arg(i + 1))); //
        }
        revision = receiver->document["rev"].toString();
    }


    CouchDbAttachmentHandler *handler = cdbClient->createAttachmentHandler(this);
    QVERIFY(handler != NULL);

    QSignalSpy attachmentSpy(handler, SIGNAL(attachmentsReady(QString,cdbqt::AttachmentDataResponse,cdbqt::AttachmentErrorResponse,QVariant)));
    QVERIFY(attachmentSpy.isValid());

    handler->getAttachments(documentId, attachmentList, QVariant(100));
    QTRY_COMPARE(attachmentSpy.count(), 1);
    QCOMPARE(attachmentSpy.at(0).at(0).toString(), documentId);

    AttachmentDataResponse attachmentsData = qvariant_cast<cdbqt::AttachmentDataResponse>(attachmentSpy.at(0).at(1));
    QCOMPARE(attachmentsData.size(), attachmentsSize);
    for(int i = 0; i < attachmentsSize; i++) {
        QString attachmentName = QString("attachment%1").arg(i);
        attachmentList.append(attachmentName);
        QVERIFY(attachmentsData.contains(attachmentName));
        QByteArray result = attachmentsData[attachmentName];
        if(i == indexOfAttachmentToSkip) {
            QCOMPARE(result.size(), 0);
        } else {
            QByteArray inputData = QString("abra-cadabra%1").arg(i).toLatin1();
            QCOMPARE(result, inputData);
        }
    }

    AttachmentErrorResponse attachmentsErrors = qvariant_cast<cdbqt::AttachmentErrorResponse>(attachmentSpy.at(0).at(2));
    QCOMPARE(attachmentsErrors.size(), 1);
    QString errorAttachmentName = attachmentsErrors.keys()[0];
    QCOMPARE(errorAttachmentName, QString("attachment%1").arg(indexOfAttachmentToSkip));
    CouchDbError::Error error = attachmentsErrors[errorAttachmentName];

    QCOMPARE(error, CouchDbError::CONTENT_NOT_FOUND);


    QCOMPARE(TestUtils::getVariantArgument(attachmentSpy.at(0), 3), QVariant(100));
//    QVariantMap attachmentsData = attachmentSpy.at(0).at(1).toMap();
//    QCOMPARE(attachmentSpy.at(0).at(2).toByteArray(), inputData);
//    QCOMPARE(TestUtils::getErrorArgument(attachmentSpy.at(0), 3), CouchDbError::NO_ERROR);
//    QCOMPARE(TestUtils::getVariantArgument(attachmentSpy.at(0), 4), QVariant(100));
}

void TestCouchDbAttachmentHandler::testGetAttachmentSpecialUrl()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_get_att/acfa342//9489021809_!()@#*/lM>?/// \\ \\// \\hment";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QVariantMap document = cdbqt::documentWithId(documentId);

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());

    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));
    QString revision = receiver->document["rev"].toString();

    QString attachmentName = "attachment123";
    QByteArray inputData = QString("abra-cadabra").toLatin1();

    QString pathToAttachment = documentId + "/" + attachmentName;
    QUrl urlToAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    urlToAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));
    spy.clear();
    cdbClient->put(receiver, urlToAttachment, inputData, QVariant(3));
    QTRY_COMPARE(spy.count(), 1);
    qDebug() << receiver->document;
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)3));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("2"));
    QString attachmentRevision = receiver->document["rev"].toString();

    CouchDbAttachmentHandler *handler = cdbClient->createAttachmentHandler(this);
    QVERIFY(handler != NULL);

    QSignalSpy attachmentSpy(handler, SIGNAL(attachmentReady(QString,QString,QByteArray,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(attachmentSpy.isValid());

    handler->getAttachment(documentId, attachmentName,  QVariant(100));
    QTRY_COMPARE(attachmentSpy.count(), 1);
    QCOMPARE(attachmentSpy.at(0).at(0).toString(), documentId);
    QCOMPARE(attachmentSpy.at(0).at(1).toString(), attachmentName);
    QCOMPARE(attachmentSpy.at(0).at(2).toByteArray(), inputData);
    QCOMPARE(TestUtils::getErrorArgument(attachmentSpy.at(0), 3), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(attachmentSpy.at(0), 4), QVariant(100));
}

void TestCouchDbAttachmentHandler::testGetAttachmentNotFound()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_get_attachment_not_found";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QVariantMap document = cdbqt::documentWithId(documentId);

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());

    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));
    QString revision = receiver->document["rev"].toString();

    QString attachmentName = "attachment123";
    QByteArray inputData = QString("abra-cadabra").toLatin1();

    QUrl urlToAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    urlToAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));
    spy.clear();
    cdbClient->put(receiver, urlToAttachment, inputData, QVariant(3));
    QTRY_COMPARE(spy.count(), 1);
    qDebug() << receiver->document;
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)3));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("2"));
    QString attachmentRevision = receiver->document["rev"].toString();

    CouchDbAttachmentHandler *handler = cdbClient->createAttachmentHandler(this);
    QVERIFY(handler != NULL);

    QSignalSpy attachmentSpy(handler, SIGNAL(attachmentReady(QString,QString,QByteArray,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(attachmentSpy.isValid());

    QString searchForAttachmentName = "lala";
    handler->getAttachment(documentId, searchForAttachmentName,  QVariant(100));
    QTRY_COMPARE(attachmentSpy.count(), 1);
    QCOMPARE(attachmentSpy.at(0).at(0).toString(), documentId);
    QCOMPARE(attachmentSpy.at(0).at(1).toString(), searchForAttachmentName);
    QCOMPARE(attachmentSpy.at(0).at(2).toByteArray().size(), 0);
    QCOMPARE(TestUtils::getErrorArgument(attachmentSpy.at(0), 3), CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getVariantArgument(attachmentSpy.at(0), 4), QVariant(100));
}
