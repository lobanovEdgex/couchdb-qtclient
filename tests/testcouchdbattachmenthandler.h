#ifndef TESTCOUCHDBATTACHMENTHANDLER_H
#define TESTCOUCHDBATTACHMENTHANDLER_H

#include <QtTest>
#include "src/couchdbattachmenthandler.h"
#include "src/utils/cdbqtmetatyperegistration.h"

class TestCouchDbAttachmentHandler : public QObject
{
    Q_OBJECT
    cdbqt::CouchDbClient *cdbClient;
public:
    explicit TestCouchDbAttachmentHandler(QObject *parent = 0);
signals:

private slots:
    void initTestCase();
    void cleanupTestCase();
    void testGetAttachment();
    void testGetAttachments();
    void testGetAttachmentSpecialUrl();
    void testGetAttachmentNotFound();
};


#endif // TESTCOUCHDBATTACHMENTHANDLER_H
