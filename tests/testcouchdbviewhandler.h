#ifndef TESTCOUCHDBVIEWHANDLER_H
#define TESTCOUCHDBVIEWHANDLER_H

#include <QtTest>
#include "src/couchdbviewhandler.h"

class TestCouchDbViewHandler : public QObject
{
    Q_OBJECT
    cdbqt::CouchDbClient *cdbClient;
public:
    explicit TestCouchDbViewHandler(QObject *parent = 0);

signals:

private slots:
    void initTestCase();
    void cleanupTestCase();
    void testViewAllDocs();
    void testView();
    void testViewCreation();

    void testViewNotExist();
};

#endif // TESTCOUCHDBVIEWHANDLER_H
