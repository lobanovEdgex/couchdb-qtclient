#include "testutils.h"

#include <QString>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTest>
#include <QSignalSpy>
#include "utils.h"
#include "src/couchdbproperties.h"
#include "src/couchdbresponsereceiver.h"

void TestUtils::deleteDatabase(cdbqt::CouchDbProperties *properties)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager();

    QUrl url;
    url.setScheme(properties->protocol());
    url.setHost(properties->host());
    url.setPort(properties->port());
    url.setPath(properties->dbName());

    QNetworkRequest request(url);

    QString concatenated = properties->username() + ":" + properties->password();
    QByteArray data = concatenated.toLocal8Bit().toBase64();
    QString headerData = "Basic " + data;
    request.setRawHeader("Authorization", headerData.toLocal8Bit());

    QNetworkReply *reply = manager->deleteResource(request);

    QSignalSpy spy(reply, SIGNAL(finished()));
    QVERIFY(spy.isValid());
    QTRY_COMPARE(spy.count(), 1);
    QVERIFY(reply->error() == QNetworkReply::ContentNotFoundError
            || reply->error() == QNetworkReply::NoError);
}

const QString TestUtils::DEFAULT_SCHEME = "http";
const QString TestUtils::DEFAULT_HOST = "127.0.0.1";
const int TestUtils::DEFAULT_PORT = 5984;
const QString TestUtils::DEFAULT_USER = "admin";
const QString TestUtils::DEFAULT_PASSWORD = "komutez#47";

cdbqt::CouchDbProperties *TestUtils::defaultPropertiesForDatabase(QString dbName)
{
    return new cdbqt::CouchDbProperties(dbName,
                                 DEFAULT_SCHEME,
                                 DEFAULT_HOST,
                                 DEFAULT_PORT,
                                 DEFAULT_USER,
                                 DEFAULT_PASSWORD,
                                        NULL);
}

QVariant TestUtils::getResponseUserData(const QList<QVariant> &arguments)
{
    return getVariantArgument(arguments, 2);
}

cdbqt::CouchDbError::Error TestUtils::getResponseError(const QList<QVariant> &arguments)
{
    return getErrorArgument(arguments, 1);
}

QVariantMap TestUtils::getResponseDocument(const QList<QVariant> &arguments)
{
    return getMapArgument(arguments, 0);
}

QVariantMap TestUtils::getMapArgument(const QList<QVariant> &arguments, const int index)
{
    QVariant variant = qvariant_cast<QVariant>(arguments.at(index));
    QVariantMap document = variant.value<QVariantMap>();

    return document;
}

cdbqt::CouchDbError::Error TestUtils::getErrorArgument(const QList<QVariant> &arguments, const int index)
{
    QVariant variant = qvariant_cast<QVariant>(arguments.at(index));
    cdbqt::CouchDbError::Error error = variant.value<cdbqt::CouchDbError::Error>();
    return error;
}

QVariant TestUtils::getVariantArgument(const QList<QVariant> &arguments, const int index)
{
    QVariant variant = qvariant_cast<QVariant>(arguments.at(index));
    QVariant userData = variant.value<QVariant>();

    return userData;
}

TestCouchdbClientReceiver::TestCouchdbClientReceiver(QObject *parent) :
    CouchDbResponseReceiver(parent)
{
}

void TestCouchdbClientReceiver::responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData)
{
    this->document = document;
    this->error = error;
    this->userData = userData;
    emit(documentReceived());
}


TestCouchdbClientRawReceiver::TestCouchdbClientRawReceiver(QObject *parent) :
    CouchDbRawResponseReceiver(parent)
{

}

void TestCouchdbClientRawReceiver::responseDataReady(const QByteArray &data, const cdbqt::CouchDbError::Error &error, const QVariant &userData)
{
    this->data = data;
    this->error = error;
    this->userData = userData;
    emit(documentReceived());
}
