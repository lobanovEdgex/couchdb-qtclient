#include <QtTest>
#include "tests/testcouchdbclient.h"
#include "tests/testcouchdbdocumenthandler.h"
#include "testcouchdbviewhandler.h"
#include "testcouchdbfilterhandler.h"
#include "testcouchdbattachmenthandler.h"
#include "tests/testcouchdbutils.h"


int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

    TestCouchDbClient testCouchDbClient;
    TestCouchDbDocumentHandler testCouchDbDocumentHandler;
    TestCouchdbUtils testCouchDbUtils;
    TestCouchDbViewHandler testCouchDbViewHandler;
    TestCouchDbFilterHandler testCouchDbFilterHandler;
    TestCouchDbAttachmentHandler testCouchDbAttachmentHandler;

    return  QTest::qExec(&testCouchDbClient, argc, argv)
            | QTest::qExec(&testCouchDbFilterHandler, argc, argv)
            | QTest::qExec(&testCouchDbViewHandler, argc, argv)
            | QTest::qExec(&testCouchDbUtils, argc, argv)
            | QTest::qExec(&testCouchDbAttachmentHandler, argc, argv)
            | QTest::qExec(&testCouchDbDocumentHandler, argc, argv);

// |           QTest::qExec(&testSuite2, argc, argv);
}
