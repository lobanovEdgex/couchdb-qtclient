#ifndef TESTUTILS_H
#define TESTUTILS_H

#include "src/couchdbresponsereceiver.h"
#include "src/couchdbrawresponsereceiver.h"

namespace cdbqt {
    class CouchDbProperties;
}

class QString;

class TestUtils
{
public:
    static const QString DEFAULT_SCHEME;
    static const QString DEFAULT_HOST;
    static const int DEFAULT_PORT;
    static const QString DEFAULT_USER;
    static const QString DEFAULT_PASSWORD;
    static void deleteDatabase(cdbqt::CouchDbProperties *couchdbProperties);
    static cdbqt::CouchDbProperties *defaultPropertiesForDatabase(QString dbName);
    static QVariant getResponseUserData(const QList<QVariant> &arguments);
    static cdbqt::CouchDbError::Error getResponseError(const QList<QVariant> &arguments);
    static QVariantMap getResponseDocument(const QList<QVariant> &arguments);

    static QVariantMap getMapArgument(const QList<QVariant> &arguments, const int index);
    static cdbqt::CouchDbError::Error getErrorArgument(const QList<QVariant> &arguments, const int index);
    static QVariant getVariantArgument(const QList<QVariant> &arguments, const int index);
};

class TestCouchdbClientReceiver : public cdbqt::CouchDbResponseReceiver
{
    Q_OBJECT
public:
    TestCouchdbClientReceiver(QObject *parent);
    QVariantMap document;
    cdbqt::CouchDbError::Error error;
    QVariant userData;
signals:
    void documentReceived();
public slots:
    void responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData);
};

class TestCouchdbClientRawReceiver : public cdbqt::CouchDbRawResponseReceiver
{
    Q_OBJECT
public:
    TestCouchdbClientRawReceiver(QObject *parent);
    QByteArray data;
    cdbqt::CouchDbError::Error error;
    QVariant userData;
signals:
    void documentReceived();
public slots:
    void responseDataReady(const QByteArray &data, const cdbqt::CouchDbError::Error &error, const QVariant &userData);
};


#endif // TESTUTILS_H
