#ifndef TESTCOUCHDBFILTERHANDLER_H
#define TESTCOUCHDBFILTERHANDLER_H

#include <QtTest>
#include "src/couchdbviewhandler.h"

class TestCouchDbFilterHandler : public QObject
{
    Q_OBJECT
    cdbqt::CouchDbClient *cdbClient;
public:
    explicit TestCouchDbFilterHandler(QObject *parent = 0);

signals:

private slots:
    void initTestCase();
    void cleanupTestCase();
    void testFilterCreation();
};

#endif // TESTCOUCHDBFILTERHANDLER_H
