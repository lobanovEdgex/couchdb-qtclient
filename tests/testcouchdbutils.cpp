#include "testcouchdbutils.h"

#include "src/couchdbutils.h"

TestCouchdbUtils::TestCouchdbUtils(QObject *parent) :
    QObject(parent)
{
}

void TestCouchdbUtils::testAddAttachment()
{
    QVariantMap document;
    const QString ATTACHMENTS_FIELD = "_attachments";
    QCOMPARE(document.count(ATTACHMENTS_FIELD), 0);
    document = cdbqt::getDocumentWithAttachment(document, "1", "data1");
    QCOMPARE(document.count(ATTACHMENTS_FIELD), 1);
    QCOMPARE(document[ATTACHMENTS_FIELD].toMap().size(), 1);
    document = cdbqt::getDocumentWithAttachment(document, "1", "data2");
    QCOMPARE(document.count(ATTACHMENTS_FIELD), 1);
    QCOMPARE(document[ATTACHMENTS_FIELD].toMap().size(), 1);
    document = cdbqt::getDocumentWithAttachment(document, "2", "data3");
    QCOMPARE(document.count(ATTACHMENTS_FIELD), 1);
    QCOMPARE(document[ATTACHMENTS_FIELD].toMap().size(), 2);
    QCOMPARE(document[ATTACHMENTS_FIELD].toMap().count("1"), 1);
    QCOMPARE(document[ATTACHMENTS_FIELD].toMap().count("2"), 1);
    QCOMPARE(document[ATTACHMENTS_FIELD].toMap()["2"].toMap()["data"].toByteArray(), QByteArray("data3").toBase64());
}

void TestCouchdbUtils::testDocumentContainsAttachment()
{
    QVariantMap document;
    const QString ATTACHMENTS_FIELD = "_attachments";
    QVERIFY(cdbqt::documentContainsAttachmentWithData(document, "attachment") == false);
    document = cdbqt::getDocumentWithAttachment(document, "1", "data1");
    QVERIFY(cdbqt::documentContainsAttachmentWithData(document, "attachment") == false);
    QVERIFY(cdbqt::documentContainsAttachmentWithData(document, "1"));
    QVariantMap attachments = document[ATTACHMENTS_FIELD].toMap();
    QVariantMap attachment = attachments["1"].toMap();
    attachment.remove("data");
    attachments["1"] = attachment;
    document[ATTACHMENTS_FIELD] = attachments;
    QVERIFY(cdbqt::documentContainsAttachmentWithData(document, "1") == false);
}

void TestCouchdbUtils::testGetDocumentAttachment()
{
    QVariantMap document;
    QByteArray data;
    for(int i = 0; i < 500; i++) {
        data.append((char)i);
    }
    QVERIFY(cdbqt::getDocumentAttachment(document, "1").size() == 0);
    document = cdbqt::getDocumentWithAttachment(document, "1", data);
    QCOMPARE(cdbqt::getDocumentAttachment(document, "1"), data);
}
