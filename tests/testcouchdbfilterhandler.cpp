#include "testcouchdbfilterhandler.h"

#include <QDebug>

#include "testutils.h"
#include "utils.h"
#include "src/couchdbproperties.h"
#include "src/couchdbclient.h"
#include "src/couchdbfilterhandler.h"

using namespace cdbqt;

TestCouchDbFilterHandler::TestCouchDbFilterHandler(QObject *parent) :
    QObject(parent)
{
}

void TestCouchDbFilterHandler::initTestCase()
{
    QString dbName = "couchdb-filter-handler-test";

    CouchDbProperties *properties = TestUtils::defaultPropertiesForDatabase(dbName);

    TestUtils::deleteDatabase(properties);

    cdbClient = new CouchDbClient(properties, this);

    QSignalSpy spy(cdbClient, SIGNAL(couchDbClientFinishedInitialization(cdbqt::CouchDbError::Error)));
    QVERIFY(spy.isValid());

    cdbClient->init();

    QTRY_COMPARE(spy.count(), 1);
    QVariant variant = qvariant_cast<QVariant>(spy.at(0).first());
    CouchDbError::Error error = variant.value<CouchDbError::Error>();
    QCOMPARE(error, CouchDbError::NO_ERROR);
}

void TestCouchDbFilterHandler::cleanupTestCase()
{

}

void TestCouchDbFilterHandler::testFilterCreation()
{
    cdbqt::CouchDbFilterHandler *filterHandler = cdbClient->createFilterHandler("_design/app", "filter", "function(doc, req) { if(doc.name == req.query.name) { return true; }  return false; }", this);
    QSignalSpy spyCreated(filterHandler, SIGNAL(filterCreated(cdbqt::CouchDbError::Error)));

    filterHandler->create();

    QTRY_COMPARE(spyCreated.count(), 1);
    QCOMPARE(TestUtils::getErrorArgument(spyCreated.first(), 0), cdbqt::CouchDbError::NO_ERROR);

    spyCreated.clear();
    filterHandler->create();

    QTRY_COMPARE(spyCreated.count(), 1);

    QCOMPARE(TestUtils::getErrorArgument(spyCreated.first(), 0), cdbqt::CouchDbError::CONFLICT);
}
