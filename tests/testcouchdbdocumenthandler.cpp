#include "testcouchdbdocumenthandler.h"
#include "testutils.h"
#include "utils.h"
#include "src/couchdbproperties.h"
#include "src/couchdbclient.h"

using namespace cdbqt;

TestCouchDbDocumentHandler::TestCouchDbDocumentHandler(QObject *parent) :
    QObject(parent)
{
}

void TestCouchDbDocumentHandler::initTestCase()
{
    QString dbName = "couchdb-document-handler-test";

    CouchDbProperties *properties = TestUtils::defaultPropertiesForDatabase(dbName);

    TestUtils::deleteDatabase(properties);

    cdbClient = new CouchDbClient(properties, this);

    QSignalSpy spy(cdbClient, SIGNAL(couchDbClientFinishedInitialization(cdbqt::CouchDbError::Error)));
    QVERIFY(spy.isValid());

    cdbClient->init();

    QTRY_COMPARE(spy.count(), 1);
    QVariant variant = qvariant_cast<QVariant>(spy.at(0).first());
    CouchDbError::Error error = variant.value<CouchDbError::Error>();
    QCOMPARE(error, CouchDbError::NO_ERROR);
}

void TestCouchDbDocumentHandler::cleanupTestCase()
{
}

void TestCouchDbDocumentHandler::testGetDocumentById()
{
    QString documentId = "testGet";
    QVariantMap document = cdbqt::documentWithId(documentId);
    document["testField"] = "test";
    QByteArray attachmentData;
    for(int i = 500; i > 0; i--) {
        attachmentData.append((char)i);
    }
    QString attachmentName = "1";
    document = getDocumentWithAttachment(document, attachmentName, attachmentData);

    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QSignalSpy putSpy(receiver, SIGNAL(documentReceived()));
    QVERIFY(putSpy.isValid());
    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(putSpy.count(), 1);

    CouchDbDocumentHandler *handler = cdbClient->createDocumentHandler(this);
    QVERIFY(handler != NULL);

    QVariant userData = "Test";

    QSignalSpy spy(handler, SIGNAL(responseReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(spy.isValid());
    handler->getDocumentWithId(documentId, false, userData);
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseUserData(spy.first()), userData);
    QVERIFY(documentContainsAttachmentWithData(TestUtils::getResponseDocument(spy.first()), attachmentName) == false);

    spy.clear();
    handler->getDocumentWithId(documentId, true, userData);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseUserData(spy.first()), userData);
    QVERIFY(documentContainsAttachmentWithData(TestUtils::getResponseDocument(spy.first()), attachmentName) == true);
    QCOMPARE(getDocumentAttachment(TestUtils::getResponseDocument(spy.first()), attachmentName), attachmentData);

    QString documentNotInBaseId = "testGetNotInBase";
    spy.clear();
    handler->getDocumentWithId(documentNotInBaseId, true, userData);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getResponseUserData(spy.first()), userData);
    QCOMPARE(TestUtils::getResponseDocument(spy.first())["_id"].toString(), documentNotInBaseId);//documentId field
}

void TestCouchDbDocumentHandler::testCreateDocument()
{
    CouchDbDocumentHandler *handler = cdbClient->createDocumentHandler(this);
    QString documentId1 = "testCreate1";
    QSignalSpy spy(handler, SIGNAL(responseReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(spy.isValid());
    handler->getDocumentWithId(documentId1, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).size(), 1);//documentId field

    spy.clear();
    QString documentId2 = "testCreate2";
    handler->getDocumentWithId(documentId2, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).size(), 1);//documentId field

    spy.clear();
    QVariantMap document = documentWithId(documentId1);
    document["testField"] = "testValue";
    QVariant userData = 231;
    handler->updateDocumentFields(document, false, false, userData);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseUserData(spy.first()), userData);
    QCOMPARE(TestUtils::getResponseDocument(spy.first())["testField"].toString(), QString("testValue"));

    spy.clear();
    handler->getDocumentWithId(documentId1, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseDocument(spy.first())["testField"].toString(), QString("testValue"));

    spy.clear();
    document = documentWithId(documentId2);
    document["testField2"] = "testValue2";
    userData = true;
    handler->updateDocumentFields(document, true, true, userData);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseUserData(spy.first()), userData);
    QCOMPARE(TestUtils::getResponseDocument(spy.first())["testField2"].toString(), QString("testValue2"));

    spy.clear();
    handler->getDocumentWithId(documentId2, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseDocument(spy.first())["testField2"].toString(), QString("testValue2"));

    spy.clear();
    QString specialSymbolsDocumentId = "specialDoc`~!\\//@#IOJавыавф аффм///\\\fs\\\fdsfds\\fdssd\\   ! @#$%^&*()_+-=[]{}|;':\",./<> ?";
    document["_id"] = specialSymbolsDocumentId;
    document["testField"] = specialSymbolsDocumentId;
    userData = true;
    handler->updateDocumentFields(document, true, true, userData);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseUserData(spy.first()), userData);
    QCOMPARE(TestUtils::getResponseDocument(spy.first())["testField"].toString(), specialSymbolsDocumentId);

    spy.clear();
    handler->getDocumentWithId(specialSymbolsDocumentId, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseDocument(spy.first())["testField"].toString(), specialSymbolsDocumentId);
}

void TestCouchDbDocumentHandler::testUpdateDocument()
{
    CouchDbDocumentHandler *handler = cdbClient->createDocumentHandler(this);
    QString documentId1 = "testUpdate";
    QSignalSpy spy(handler, SIGNAL(responseReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(spy.isValid());
    handler->getDocumentWithId(documentId1, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).size(), 1); //documentIdField

    //testst with clearOldFields = false merge = false
    spy.clear();
    QVariantMap document = documentWithId(documentId1);
    for(int i = 0; i < 3; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, false, false, userData);
    }
    QTRY_COMPARE(spy.count(), 3);
    for(int i = 0; i < 3; i++) {
        if(i == 1) {
            //for testing merge conflicts
            document = TestUtils::getResponseDocument(spy.at(i));
        }
        QCOMPARE(TestUtils::getResponseError(spy.at(i)), CouchDbError::NO_ERROR);
        QCOMPARE(TestUtils::getResponseUserData(spy.at(i)).toInt(), i);
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i))[QString("testField%1").arg(i)].toInt(), i);
    }
    spy.clear();
    handler->getDocumentWithId(documentId1, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).count(), 5); //3 testField's + id + rev
    //test merge conflicts
    spy.clear();
    for(int i = 3; i < 6; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, false, false, userData);
    }
    QTRY_COMPARE(spy.count(), 3);
    for(int i = 3; i < 6; i++) {
        QCOMPARE(TestUtils::getResponseError(spy.at(i-3)), CouchDbError::CONFLICT);
        QCOMPARE(TestUtils::getResponseUserData(spy.at(i-3)).toInt(), i);
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i-3)).count(), 5); //3 testField's + id + rev (last good revision)
    }
    //test override merge
    spy.clear();
    for(int i = 6; i < 9; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, false, true, userData);
    }
    QTRY_COMPARE(spy.count(), 3);
    for(int i = 6; i < 9; i++) {
        QCOMPARE(TestUtils::getResponseError(spy.at(i-6)), CouchDbError::NO_ERROR);
        QCOMPARE(TestUtils::getResponseUserData(spy.at(i-6)).toInt(), i);
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i-6))[QString("testField%1").arg(i)].toInt(), i);
//        QCOMPARE(TestUtils::getResponseDocument(spy.at(i-6)).count(), 5); //3 testField's + id + rev (last good revision)
    }
    //test clearOldFields
    spy.clear();
    for(int i = 9; i < 12; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, true, false, userData);
    }
    document.clear();
    QTRY_COMPARE(spy.count(), 3);
    for(int i = 9; i < 12; i++) {
        QCOMPARE(TestUtils::getResponseError(spy.at(i-9)), CouchDbError::CONFLICT);
        QCOMPARE(TestUtils::getResponseUserData(spy.at(i-9)).toInt(), i);
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i-9)).count(), 11); //9 testField's + id + rev (last good revision)
    }
    //test clearOldFields
    document = documentWithId(documentId1);
    spy.clear();
    for(int i = 12; i < 15; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, true, true, userData);
    }
    QTRY_COMPARE(spy.count(), 3);
    for(int i = 12; i < 15; i++) {
        QCOMPARE(TestUtils::getResponseError(spy.at(i-12)), CouchDbError::NO_ERROR);
        QCOMPARE(TestUtils::getResponseUserData(spy.at(i-12)).toInt(), i);
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i-12)).count(), i-12+2+1); //testField's + id + rev (last good revision)
    }
}

void TestCouchDbDocumentHandler::testRemoveAttachmentUpdateDocument()
{
    CouchDbDocumentHandler *handler = cdbClient->createDocumentHandler(this);
    QString documentId = "testRemoveAttachment";
    QSignalSpy spy(handler, SIGNAL(responseReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(spy.isValid());
    handler->getDocumentWithId(documentId, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).size(), 1); //documentIdField
    spy.clear();

    QVariantMap document = documentWithId(documentId);
    handler->updateDocumentFields(document, false, false, QVariant(0));
    QTRY_COMPARE(spy.count(), 1);
    qDebug() << TestUtils::getResponseError(spy.first());
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QString revision =  TestUtils::getResponseDocument(spy.first())["_rev"].toString();
    //add attachment
    spy.clear();
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);

    QSignalSpy spyPutAttachment(receiver, SIGNAL(documentReceived()));
    QVERIFY(spyPutAttachment.isValid());
    spyPutAttachment.clear();
    QString attachmentName = "attachment";
    QByteArray inputData = QString("123").toLatin1();

    QString pathToAttachment = documentId + "/" + attachmentName;
    QUrl urlToAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    urlToAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));
    cdbClient->put(receiver, urlToAttachment, inputData, QVariant(3));
    QTRY_COMPARE(spyPutAttachment.count(), 1);
    qDebug() << receiver->document;
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)3));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("2"));

    handler->getDocumentWithId(documentId, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).size(), 3); //documentId, revision and attachments fields
    document = TestUtils::getResponseDocument(spy.first());
    spy.clear();

    qDebug() << document;

    for(int i = 0; i < 3; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, false, true, userData);
    }
    //i == 3
    handler->deleteAttachment(documentId, attachmentName, QVariant(3));
    for(int i = 4; i < 7; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, false, true, userData);
    }
    handler->deleteAttachment(documentId, attachmentName, QVariant(7));
    QTRY_COMPARE(spy.count(), 8);
    for(int i = 0; i < 7; i++) {
        qDebug() << i;
        qDebug() << TestUtils::getResponseError(spy.at(i));
        QCOMPARE(TestUtils::getResponseError(spy.at(i)), CouchDbError::NO_ERROR);
        QCOMPARE(TestUtils::getResponseUserData(spy.at(i)).toInt(), i);
//        QCOMPARE(TestUtils::getResponseDocument(spy.at()).count(), i); //3 testField's + id + rev (last good revision)
    }
    for(int i = 0; i < 3; i++) {
        //id + rev + attachments
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i)).count(), 3 + i + 1);
    }
    //id + rev + testField0 + testField1 + testField2
    QCOMPARE(TestUtils::getResponseDocument(spy.at(3)).count(), 5);
    for(int i = 4; i < 7; i++) {
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i)).count(), 5 + i + 1 - 4);
    }
    qDebug() <<TestUtils::getResponseError(spy.at(7));
    //couchdb doesn't test if there's really an attachment
    QCOMPARE(TestUtils::getResponseError(spy.at(7)), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseUserData(spy.at(7)).toInt(), 7);
    QCOMPARE(TestUtils::getResponseDocument(spy.at(7)).count(), 8);

    spy.clear();

    QUrl urlToGetAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    TestCouchdbClientRawReceiver *rawReceiver = new TestCouchdbClientRawReceiver(this);
    QSignalSpy rawSpy(rawReceiver, SIGNAL(documentReceived()));
    QVERIFY(rawSpy.isValid());
    rawSpy.clear();
    cdbClient->get(rawReceiver ,urlToGetAttachment, QVariant("getAttachment"));
    QTRY_COMPARE(rawSpy.count(), 1);
    qDebug() << rawReceiver->data;
    QCOMPARE(rawReceiver->error, CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(rawReceiver->userData, QVariant("getAttachment"));
    QByteArray testData;
    QCOMPARE(rawReceiver->data, testData);
}

void TestCouchDbDocumentHandler::testRemoveAttachmentSpecialCharactersUpdateDocument()
{
    CouchDbDocumentHandler *handler = cdbClient->createDocumentHandler(this);
    QString documentId = "testRemoveAttachmentSpecial10928309y83629-0!@)_(#*!@*&$^*%^// / f\\ fd j90s8901 / /fj `[ u9012\\";
    QSignalSpy spy(handler, SIGNAL(responseReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(spy.isValid());
    handler->getDocumentWithId(documentId, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).size(), 1); //documentIdField
    spy.clear();

    QVariantMap document = documentWithId(documentId);
    handler->updateDocumentFields(document, false, false, QVariant(0));
    QTRY_COMPARE(spy.count(), 1);
    qDebug() << TestUtils::getResponseError(spy.first());
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QString revision =  TestUtils::getResponseDocument(spy.first())["_rev"].toString();
    //add attachment
    spy.clear();
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);

    QSignalSpy spyPutAttachment(receiver, SIGNAL(documentReceived()));
    QVERIFY(spyPutAttachment.isValid());
    spyPutAttachment.clear();
    QString attachmentName = "attachment/213/|\\ / \\  /";
    QByteArray inputData = QString("123").toLatin1();

    QString pathToAttachment = documentId + "/" + attachmentName;
    QUrl urlToAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    urlToAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));
    cdbClient->put(receiver, urlToAttachment, inputData, QVariant(3));
    QTRY_COMPARE(spyPutAttachment.count(), 1);
    qDebug() << receiver->document;
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)3));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("2"));

    handler->getDocumentWithId(documentId, false);
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(TestUtils::getResponseError(spy.first()), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseDocument(spy.first()).size(), 3); //documentId, revision and attachments fields
    document = TestUtils::getResponseDocument(spy.first());
    spy.clear();

    qDebug() << document;

    for(int i = 0; i < 3; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, false, true, userData);
    }
    //i == 3
    handler->deleteAttachment(documentId, attachmentName, QVariant(3));
    for(int i = 4; i < 7; i++) {
        document[QString("testField%1").arg(i)] = i;
        QVariant userData = i;
        handler->updateDocumentFields(document, false, true, userData);
    }
    handler->deleteAttachment(documentId, attachmentName, QVariant(7));
    QTRY_COMPARE(spy.count(), 8);
    for(int i = 0; i < 7; i++) {
        qDebug() << i;
        qDebug() << TestUtils::getResponseError(spy.at(i));
        QCOMPARE(TestUtils::getResponseError(spy.at(i)), CouchDbError::NO_ERROR);
        QCOMPARE(TestUtils::getResponseUserData(spy.at(i)).toInt(), i);
//        QCOMPARE(TestUtils::getResponseDocument(spy.at()).count(), i); //3 testField's + id + rev (last good revision)
    }
    for(int i = 0; i < 3; i++) {
        //id + rev + attachments
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i)).count(), 3 + i + 1);
    }
    //id + rev + testField0 + testField1 + testField2
    QCOMPARE(TestUtils::getResponseDocument(spy.at(3)).count(), 5);
    for(int i = 4; i < 7; i++) {
        QCOMPARE(TestUtils::getResponseDocument(spy.at(i)).count(), 5 + i + 1 - 4);
    }
    qDebug() <<TestUtils::getResponseError(spy.at(7));
    //couchdb doesn't test if there's really an attachment
    QCOMPARE(TestUtils::getResponseError(spy.at(7)), CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getResponseUserData(spy.at(7)).toInt(), 7);
    QCOMPARE(TestUtils::getResponseDocument(spy.at(7)).count(), 8);

    spy.clear();

    QUrl urlToGetAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    TestCouchdbClientRawReceiver *rawReceiver = new TestCouchdbClientRawReceiver(this);
    QSignalSpy rawSpy(rawReceiver, SIGNAL(documentReceived()));
    QVERIFY(rawSpy.isValid());
    rawSpy.clear();
    cdbClient->get(rawReceiver ,urlToGetAttachment, QVariant("getAttachment"));
    QTRY_COMPARE(rawSpy.count(), 1);
    qDebug() << rawReceiver->data;
    QCOMPARE(rawReceiver->error, CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(rawReceiver->userData, QVariant("getAttachment"));
    QByteArray testData;
    QCOMPARE(rawReceiver->data, testData);
}

void TestCouchDbDocumentHandler::testCleanup()
{
    int numberOfDocuments = 8;
    int numberOfIterations = 4;
    CouchDbDocumentHandler *handler = new CouchDbDocumentHandler(cdbClient, this, 1, numberOfDocuments);
    //in the end must have not more than 2 documents tracked

    QSignalSpy spy(handler, SIGNAL(responseReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    QVERIFY(spy.isValid());


    int numberOfUpdates = 0;
    //number of iterations
    for(int i = 0; i < numberOfIterations; i++) {
        //select document to update
        for(int j = 0; j < numberOfDocuments; j++) {
            //timesToUpdateDocument (in the end document with j must have 'field' value equal j)
            for(int k = 0; k <= j; k++) {
                numberOfUpdates++;
                QVariantMap document = documentWithId(QString("testCleanup%1").arg(j));
                document["field"] = k;
                handler->updateDocumentFields(document);
            }
        }
        QTRY_COMPARE(spy.count(), numberOfUpdates);
        QVERIFY(handler->currentTrackedDocumentsCount() <= 2);
    }
    for(int i = 0; i < numberOfDocuments; i++) {
        spy.clear();
        handler->getDocumentWithId(QString("testCleanup%1").arg(i));
        QTRY_COMPARE(spy.count(), 1);
        QVariantMap document = TestUtils::getResponseDocument(spy.first());
        QCOMPARE(document["field"].toInt(), i);
        QVERIFY(document["_rev"].toString().startsWith(QString::number(numberOfIterations * (i + 1))));
    }
}
