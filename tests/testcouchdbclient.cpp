#include "testcouchdbclient.h"
#include "testutils.h"

#include <QSignalSpy>
#include <QVariant>
#include <QUrl>
#include <QNetworkAccessManager>
#include "testutils.h"
#include "utils.h"

using namespace cdbqt;

void TestCouchDbClient::initTestCase()
{
    QString dbName = "couchdb-client-test";

    CouchDbProperties *properties = TestUtils::defaultPropertiesForDatabase(dbName);

    TestUtils::deleteDatabase(properties);

    cdbClient = new CouchDbClient(properties, this);

    QCOMPARE(cdbClient->getBaseUrl().host(), TestUtils::DEFAULT_HOST);
    QCOMPARE(cdbClient->getBaseUrl().port(), TestUtils::DEFAULT_PORT);
    QCOMPARE(cdbClient->getBaseUrl().scheme(), TestUtils::DEFAULT_SCHEME);
    QString basePath = "/" + dbName;
    QString documentId = "docId";
    QString path = basePath + "/" + documentId;
//    QCOMPARE(cdbClient->getPathForCurrentDatabase(documentId), path);
    documentId = "docId31127808";
    path = basePath + "/" + documentId;
//    QCOMPARE(cdbClient->getPathForCurrentDatabase(documentId), path);

    QSignalSpy spy(cdbClient, SIGNAL(couchDbClientFinishedInitialization(cdbqt::CouchDbError::Error)));
    QVERIFY(spy.isValid());

    cdbClient->init();

    QTRY_COMPARE(spy.count(), 1);
    QVariant variant = qvariant_cast<QVariant>(spy.at(0).first());
    CouchDbError::Error error = variant.value<CouchDbError::Error>();
    QCOMPARE(error, CouchDbError::NO_ERROR);
}

void TestCouchDbClient::cleanupTestCase()
{
}

void TestCouchDbClient::init()
{
}

void TestCouchDbClient::cleanup()
{
}

void TestCouchDbClient::testDocumentCreation()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_c";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());
    cdbClient->get(receiver, documentUrl, QVariant((int)8));
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(receiver->error, CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(receiver->userData, QVariant((int)8));
    QCOMPARE(receiver->document.count(), 0);

    QVariantMap document = cdbqt::documentWithId(documentId);
    int testValueInteger = 175;
    double testValueDouble = 175.3216091;
    QString testValueString = "testValue";
    document["testFieldInteger"] = testValueInteger;
    document["testFieldDouble"] = testValueDouble;
    document["testFieldString"] = testValueString;

    spy.clear();
    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));

    spy.clear();
    cdbClient->get(receiver, documentUrl, document);
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);

    QCOMPARE(receiver->document["testFieldInteger"].toInt(), testValueInteger);
    QCOMPARE(receiver->document["testFieldDouble"].toDouble(), testValueDouble);
    QCOMPARE(receiver->document["testFieldString"].toString(), testValueString);
}

void TestCouchDbClient::testDocumentDeletion()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_d";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());
    cdbClient->get(receiver, documentUrl, QVariant((int)9));
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(receiver->error, CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(receiver->userData, QVariant((int)9));
    QCOMPARE(receiver->document.count(), 0);

    QVariantMap document = cdbqt::documentWithId(documentId);
    int testValueInteger = 175;
    double testValueDouble = 175.3216091;
    QString testValueString = "testValue";
    document["testFieldInteger"] = testValueInteger;
    document["testFieldDouble"] = testValueDouble;
    document["testFieldString"] = testValueString;

    spy.clear();
    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));
    QString revision = receiver->document["rev"].toString();

    QString pathToDelete = documentId;
    QUrl urlToDelete = cdbClient->getUrlForPathForCurrentDatabase(pathToDelete);
    urlToDelete.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));
    spy.clear();
    cdbClient->deleteResource(receiver, urlToDelete, QVariant((int)116));
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)116));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("2"));

    spy.clear();
    cdbClient->get(receiver, documentUrl, QVariant((int)9));
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(receiver->error, CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(receiver->userData, QVariant((int)9));
    QCOMPARE(receiver->document.count(), 0);
}

void TestCouchDbClient::testPutGetDeleteAttachment()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_ga";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());
    cdbClient->get(receiver, documentUrl, QVariant((int)9));
    QTRY_COMPARE(spy.count(), 1);
    QCOMPARE(receiver->error, CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(receiver->userData, QVariant((int)9));
    QCOMPARE(receiver->document.count(), 0);

    QVariantMap document = cdbqt::documentWithId(documentId);

    spy.clear();
    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));
    QString revision = receiver->document["rev"].toString();

    QString attachmentName = "attachment";
    QByteArray inputData = QString("123").toLatin1();

    QString pathToAttachment = documentId + "/" + attachmentName;
    QUrl urlToAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    urlToAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(revision));
    spy.clear();
    cdbClient->put(receiver, urlToAttachment, inputData, QVariant(3));
    QTRY_COMPARE(spy.count(), 1);
    qDebug() << receiver->document;
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)3));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("2"));
    QString attachmentRevision = receiver->document["rev"].toString();

    QUrl urlToGetAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    TestCouchdbClientRawReceiver *rawReceiver = new TestCouchdbClientRawReceiver(this);
    QSignalSpy rawSpy(rawReceiver, SIGNAL(documentReceived()));
    QVERIFY(rawSpy.isValid());
    rawSpy.clear();
    cdbClient->get(rawReceiver ,urlToGetAttachment, QVariant("getAttachment"));
    QTRY_COMPARE(rawSpy.count(), 1);
    qDebug() << rawReceiver->data;
    QCOMPARE(rawReceiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(rawReceiver->userData, QVariant("getAttachment"));
    QByteArray testData = QString("123").toLatin1();
    QCOMPARE(rawReceiver->data, testData);

    QUrl urlToDeleteAttachment = cdbClient->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentName);
    urlToDeleteAttachment.addEncodedQueryItem("rev", QUrl::toPercentEncoding(attachmentRevision));
    spy.clear();
    cdbClient->deleteResource(receiver, urlToDeleteAttachment, QVariant((int)8));
    qDebug() << urlToDeleteAttachment;
    QTRY_COMPARE(spy.count(), 1);
    qDebug() << receiver->document;
    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant((int)8));
    QCOMPARE(receiver->document.count(), 3);
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QCOMPARE(receiver->document["ok"].toBool(), true);
    QVERIFY(receiver->document["rev"].toString().startsWith("3"));
    attachmentRevision = document["rev"].toString();

    rawSpy.clear();
    qDebug() << urlToGetAttachment;
    cdbClient->get(rawReceiver ,urlToGetAttachment, QVariant("getAttachmentReq"));
    QTRY_COMPARE(rawSpy.count(), 1);
    qDebug() << rawReceiver->data;
    QCOMPARE(rawReceiver->error, CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(rawReceiver->userData, QVariant("getAttachmentReq"));
    QVERIFY(rawReceiver->data.isEmpty());
}

void TestCouchDbClient::testDocumentCreationConflict()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QString documentId = "test_conflict";
    QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);

    QVariantMap document = cdbqt::documentWithId(documentId);
    document["testField"] = "1";

    QSignalSpy spy(receiver, SIGNAL(documentReceived()));
    QVERIFY(spy.isValid());

    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
    QCOMPARE(receiver->document["id"].toString(), documentId);
    QVERIFY(receiver->document["rev"].toString().startsWith("1"));

    spy.clear();
    document["testField"] = "2";
    cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    QTRY_COMPARE(spy.count(), 1);

    QCOMPARE(receiver->error, CouchDbError::CONFLICT);
    QCOMPARE(receiver->userData, QVariant("testVariant"));
//    QCOMPARE(receiver->document["id"].toString(), documentId);
//    QVERIFY(receiver->document["rev"].toString().startsWith("1"));
    cdbClient->get(receiver, documentUrl, document);
}

void TestCouchDbClient::testCouchDbChangesetListener()
{
    QSignalSpy listenerSpy(cdbClient, SIGNAL(documentChanged(QVariantMap,cdbqt::CouchDbError::Error)));
    QVERIFY(listenerSpy.isValid());
    cdbClient->startChangesetListening();
    QTest::qWait(100);
    QTRY_COMPARE(listenerSpy.count(), 0);
    QString documentId = "test_changeset";
    QString revision = "";

    //1000 revisions of 1 document
    const int numberOfRevisions = 5;
    for(int i = 0; i < numberOfRevisions; i++) {
        TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
        QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);
        QVariantMap document = cdbqt::documentWithIdAndRevision(documentId, revision);
        document["i"] = i;
        QSignalSpy spy(receiver, SIGNAL(documentReceived()));
        QVERIFY(spy.isValid());
        cdbClient->put(receiver, documentUrl, document, QVariant((int)8));
        QTRY_COMPARE(spy.count(), 1);
        QCOMPARE(receiver->error, CouchDbError::NO_ERROR);
        QCOMPARE(receiver->userData, QVariant((int)8));
        QCOMPARE(receiver->document["ok"].toBool(), true);
        revision = receiver->document["rev"].toString();
    }
    QTRY_COMPARE(listenerSpy.count(), numberOfRevisions);
    for(int i = 0; i < numberOfRevisions; i++) {
        QVariant variant = qvariant_cast<QVariant>(listenerSpy.at(i).first());
        QVariantMap document = variant.value<QVariantMap>();
        int value = document["i"].toInt();
        QTRY_COMPARE(value, i);
    }
    listenerSpy.clear();
    int numberOfDocuments = 5;
    for(int i = 0; i < numberOfDocuments; i++) {
        TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
        QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId + QString::number(i));
        QVariantMap document = cdbqt::documentWithIdAndRevision(documentId, revision);
//        QSignalSpy spy(receiver, SIGNAL(documentReceived()));
//        QVERIFY(spy.isValid());
        cdbClient->put(receiver, documentUrl, document, QVariant((int)8));
//        QTRY_COMPARE(spy.count(), 1);
//        QCOMPARE(receiver->error, CouchDbResponseReceiver::NO_ERROR);
//        QCOMPARE(receiver->userData, QVariant((int)8));
//        QCOMPARE(receiver->document["ok"].toBool(), true);
//        revision = receiver->document["rev"].toString();
    }
    QTRY_COMPARE(listenerSpy.count(), numberOfDocuments);
}
