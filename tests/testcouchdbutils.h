#ifndef TESTCOUCHDBUTILS_H
#define TESTCOUCHDBUTILS_H

#include <QtTest>
#include "src/couchdbclient.h"


class TestCouchdbUtils : public QObject
{
    Q_OBJECT
public:
    explicit TestCouchdbUtils(QObject *parent = 0);

private slots:
    void testAddAttachment();
    void testDocumentContainsAttachment();
    void testGetDocumentAttachment();
};

#endif // TESTCOUCHDBUTILS_H
