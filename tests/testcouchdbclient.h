#ifndef TESTCOUCHDBCLIENT_H
#define TESTCOUCHDBCLIENT_H

#include <QtTest>
#include "src/couchdbclient.h"

class TestCouchDbClient : public QObject
{
    Q_OBJECT
private:
    cdbqt::CouchDbClient *cdbClient;
private slots:
    void initTestCase();
    void cleanupTestCase();

    void init();
    void cleanup();

    void testDocumentCreation();
    void testDocumentDeletion();
    void testPutGetDeleteAttachment();
    void testDocumentCreationConflict();
    void testCouchDbChangesetListener();
};
#endif // TESTCOUCHDBCLIENT_H
