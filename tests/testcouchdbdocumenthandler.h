#ifndef TESTCOUCHDBDOCUMENTHANDLER_H
#define TESTCOUCHDBDOCUMENTHANDLER_H

#include <QtTest>
#include "src/couchdbdocumenthandler.h"

class TestCouchDbDocumentHandler : public QObject
{
    Q_OBJECT
    cdbqt::CouchDbClient *cdbClient;
public:
    explicit TestCouchDbDocumentHandler(QObject *parent = 0);
private slots:
    void initTestCase();
    void cleanupTestCase();

    void testGetDocumentById();
    void testCreateDocument();
    void testUpdateDocument();
    void testRemoveAttachmentUpdateDocument();
    void testRemoveAttachmentSpecialCharactersUpdateDocument();
    void testCleanup();
};

#endif // TESTCOUCHDBDOCUMENTHANDLER_H
