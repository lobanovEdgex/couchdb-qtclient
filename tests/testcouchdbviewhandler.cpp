#include "testcouchdbviewhandler.h"
#include "testutils.h"
#include "utils.h"
#include "src/couchdbproperties.h"
#include "src/couchdbclient.h"
#include "src/couchdbviewhandler.h"

using namespace cdbqt;

TestCouchDbViewHandler::TestCouchDbViewHandler(QObject *parent) :
    QObject(parent)
{
}

void TestCouchDbViewHandler::initTestCase()
{
    QString dbName = "couchdb-view-handler-test";

    CouchDbProperties *properties = TestUtils::defaultPropertiesForDatabase(dbName);

    TestUtils::deleteDatabase(properties);

    cdbClient = new CouchDbClient(properties, this);

    QSignalSpy spy(cdbClient, SIGNAL(couchDbClientFinishedInitialization(cdbqt::CouchDbError::Error)));
    QVERIFY(spy.isValid());

    cdbClient->init();

    QTRY_COMPARE(spy.count(), 1);
    QVariant variant = qvariant_cast<QVariant>(spy.at(0).first());
    CouchDbError::Error error = variant.value<CouchDbError::Error>();
    QCOMPARE(error, CouchDbError::NO_ERROR);
}

void TestCouchDbViewHandler::cleanupTestCase()
{
}

void TestCouchDbViewHandler::testViewCreation()
{
    CouchDbViewHandler *viewHandler = cdbClient->createViewHandler("/_design/test/_view/test", this);
    QSignalSpy spyDocumentReady(viewHandler, SIGNAL(viewDocumentReady(QVariantMap,QVariant,QVariant,cdbqt::CouchDbError::Error,QVariant)));
    QSignalSpy spyNoMoreRows(viewHandler, SIGNAL(noMoreRows(cdbqt::CouchDbError::Error,QVariant)));
    QVariant userData = 123;

    viewHandler->run(true, userData);

    QTRY_COMPARE(spyNoMoreRows.count(), 1);
    //no rows were emited
    QCOMPARE(spyDocumentReady.count(), 0);
    QCOMPARE(TestUtils::getErrorArgument(spyNoMoreRows.first(), 0), cdbqt::CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getVariantArgument(spyNoMoreRows.first(), 1), userData);

    viewHandler = cdbClient->createViewHandler("_design/test", "test",
                                               "function(doc) { if(doc._id == '123') emit(doc._id, nil); }",
                                               this);
    QSignalSpy spyNoMoreRowsCreation(viewHandler, SIGNAL(noMoreRows(cdbqt::CouchDbError::Error,QVariant)));
    viewHandler->run(true, userData);
    QTRY_COMPARE(spyNoMoreRowsCreation.count(), 1);
    QCOMPARE(TestUtils::getErrorArgument(spyNoMoreRowsCreation.first(), 0), cdbqt::CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(spyNoMoreRowsCreation.first(), 1), userData);

    viewHandler = cdbClient->createViewHandler("/_design/test/_view/test", this);
    QSignalSpy spyNoMoreRowsNew(viewHandler, SIGNAL(noMoreRows(cdbqt::CouchDbError::Error,QVariant)));
    viewHandler->run(true, userData);
    QTRY_COMPARE(spyNoMoreRowsNew.count(), 1);
    QCOMPARE(TestUtils::getErrorArgument(spyNoMoreRowsNew.first(), 0), cdbqt::CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(spyNoMoreRowsNew.first(), 1), userData);
}

void TestCouchDbViewHandler::testViewNotExist()
{
    CouchDbViewHandler *viewHandler = cdbClient->createViewHandler("wrongView", this);
    QSignalSpy spyDocumentReady(viewHandler, SIGNAL(viewDocumentReady(QVariantMap,QVariant,QVariant,cdbqt::CouchDbError::Error,QVariant)));
    QSignalSpy spyNoMoreRows(viewHandler, SIGNAL(noMoreRows(cdbqt::CouchDbError::Error,QVariant)));
    QVariant userData = 123;

    viewHandler->run(true, userData);

    QTRY_COMPARE(spyNoMoreRows.count(), 1);
    //no rows were emited
    QCOMPARE(spyDocumentReady.count(), 0);
    QCOMPARE(TestUtils::getErrorArgument(spyNoMoreRows.first(), 0), cdbqt::CouchDbError::CONTENT_NOT_FOUND);
    QCOMPARE(TestUtils::getVariantArgument(spyNoMoreRows.first(), 1), userData);
}

void TestCouchDbViewHandler::testViewAllDocs()
{
    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QSignalSpy putSpy(receiver, SIGNAL(documentReceived()));
    QVERIFY(putSpy.isValid());

    const int NUMBER_OF_DOCS = 5;
    for(int i = 0; i < NUMBER_OF_DOCS; i++) {
        QString documentId = QString("testViewAllDocsDocument%1").arg(i);
        QVariantMap document = documentWithId(documentId);
        document["field"] = i;
        QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);
        cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    }
    QTRY_COMPARE(putSpy.count(), NUMBER_OF_DOCS);

    CouchDbViewHandler *handler = cdbClient->createViewHandler(CouchDbViewHandler::SPECIAL_VIEW_ALL_DOCS);
    QVariant userData = "userData";
    QSignalSpy spy(handler, SIGNAL(viewDocumentReady(QVariantMap,QVariant,QVariant,cdbqt::CouchDbError::Error,QVariant)));
    QSignalSpy noMoreRowsSpy(handler, SIGNAL(noMoreRows(cdbqt::CouchDbError::Error,QVariant)));
    handler->run(false, userData);
    QTRY_COMPARE(spy.count(), NUMBER_OF_DOCS);
    QSet<int> ids;
    for(int i = 0; i < NUMBER_OF_DOCS; i++) {
        QCOMPARE(TestUtils::getMapArgument(spy.at(i), 0).size(), 1); //only document id
        QVERIFY(TestUtils::getVariantArgument(spy.at(i), 1).toString().startsWith("testViewAllDocsDocument")); //key contains document id
        QString documentId = TestUtils::getVariantArgument(spy.at(i), 1).toString();
        int id = documentId.mid(QString("testViewAllDocsDocument").size()).toInt();
        ids.insert(id);
        QCOMPARE(TestUtils::getMapArgument(spy.at(i), 0)["_id"].toString(), documentId); //only document id

        QCOMPARE(TestUtils::getVariantArgument(spy.at(i), 2).toMap().count("rev"), 1); //key contains document id
        QCOMPARE(TestUtils::getErrorArgument(spy.at(i), 3), cdbqt::CouchDbError::NO_ERROR);
        QCOMPARE(TestUtils::getVariantArgument(spy.at(i), 4), userData);
    }
    QCOMPARE(ids.size(), NUMBER_OF_DOCS);
    QTRY_COMPARE(noMoreRowsSpy.count(), 1);
    QCOMPARE(TestUtils::getErrorArgument(noMoreRowsSpy.first(), 0), cdbqt::CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(noMoreRowsSpy.first(), 1), userData);

    ids.clear();
    spy.clear();
    noMoreRowsSpy.clear();
    handler->run(true, userData);
    QTRY_COMPARE(spy.count(), NUMBER_OF_DOCS);
    for(int i = 0; i < NUMBER_OF_DOCS; i++) {
        qDebug() << (TestUtils::getVariantArgument(spy.at(i), 1).toString());
        QString documentId = TestUtils::getVariantArgument(spy.at(i), 1).toString();
        int id = documentId.mid(QString("testViewAllDocsDocument").size()).toInt();
        ids.insert(id);
        QCOMPARE(TestUtils::getMapArgument(spy.at(i), 0).size(), 3); //only document id + rev + field
        QCOMPARE(TestUtils::getMapArgument(spy.at(i), 0)["field"].toInt(), id);
        QVERIFY(TestUtils::getVariantArgument(spy.at(i), 1).toString().startsWith("testViewAllDocsDocument")); //key contains document id
        QCOMPARE(TestUtils::getVariantArgument(spy.at(i), 2).toMap().count("rev"), 1);
        QCOMPARE(TestUtils::getErrorArgument(spy.at(i), 3), cdbqt::CouchDbError::NO_ERROR);
        QCOMPARE(TestUtils::getVariantArgument(spy.at(i), 4), userData);
    }
    QTRY_COMPARE(noMoreRowsSpy.count(), 1);
    QCOMPARE(TestUtils::getErrorArgument(noMoreRowsSpy.first(), 0), cdbqt::CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(noMoreRowsSpy.first(), 1), userData);
}

void TestCouchDbViewHandler::testView()
{
    CouchDbViewHandler *viewHandler = cdbClient->createViewHandler("_design/view", "view",
                                               "function(doc) { if(doc.value == 2  && doc._id.indexOf('viewTest') == 0) emit(doc._id, doc) }",
                                               this);
    QSignalSpy spyNoMoreRows(viewHandler, SIGNAL(noMoreRows(cdbqt::CouchDbError::Error,QVariant)));
    QSignalSpy spyDocumentReady(viewHandler, SIGNAL(viewDocumentReady(QVariantMap,QVariant,QVariant,cdbqt::CouchDbError::Error,QVariant)));
    QVariant userData = "userData";
    viewHandler->run(true, userData);
    QTRY_COMPARE(spyNoMoreRows.count(), 1);
    QCOMPARE(TestUtils::getErrorArgument(spyNoMoreRows.first(), 0), cdbqt::CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(spyNoMoreRows.first(), 1), userData);
    QCOMPARE(spyDocumentReady.count(), 0);

    spyDocumentReady.clear();
    spyNoMoreRows.clear();

    TestCouchdbClientReceiver *receiver = new TestCouchdbClientReceiver(this);
    QSignalSpy putSpy(receiver, SIGNAL(documentReceived()));
    QVERIFY(putSpy.isValid());

    const int NUMBER_OF_DOCS = 10;
    int expectedDocsCount = 0;
    for(int i = 0; i < NUMBER_OF_DOCS; i++) {
        QString documentId = QString("viewTestDocument%1").arg(i);
        QVariantMap document = documentWithId(documentId);
        int value = i % 3;
        if(value == 2) {
            expectedDocsCount++;
        }
        document["value"] = value;
        QUrl documentUrl = cdbClient->getUrlForPathForCurrentDatabase(documentId);
        cdbClient->put(receiver, documentUrl, document, QVariant("testVariant"));
    }
    QTRY_COMPARE(putSpy.count(), NUMBER_OF_DOCS);

    spyNoMoreRows.clear();
    spyDocumentReady.clear();
    viewHandler->run(true, userData);
    QTRY_COMPARE(spyNoMoreRows.count(), 1);
    QTRY_COMPARE(spyDocumentReady.count(), expectedDocsCount);
    QCOMPARE(TestUtils::getErrorArgument(spyNoMoreRows.first(), 0), cdbqt::CouchDbError::NO_ERROR);
    QCOMPARE(TestUtils::getVariantArgument(spyNoMoreRows.first(), 1), userData);
    QCOMPARE(spyDocumentReady.count(), expectedDocsCount);
}
