# CouchDB-QtClient: asynchronous framework for working with couch db in qt style #

CouchDb is a simple framework that tries to be as much asynchronous as possible. Work with attachments, continuous changes, asynchronous document updates. Multi-thread safe.

#Dependencies:
* libqjson
* cutelogger https://gitorious.org/cutelogger