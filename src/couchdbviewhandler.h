#ifndef COUCHDBVIEWHANDLER_H
#define COUCHDBVIEWHANDLER_H

#include "couchdbresponsereceiver.h"

namespace cdbqt {

class CouchDbClient;

/** Class for handling couchdb views.
 * Current version can emit results of given view, with document bodies if needed.
 */
class CouchDbViewHandler : public CouchDbResponseReceiver
{
    Q_OBJECT
    static const QString EXTERNAL_USER_DATA;
    static const QString INCLUDE_DOCUMENT_BODY_DATA;
    static const QString PURPOSE_DATA;
    static const QString KEY_DATA;
    static const QString VALUE_DATA;
    static const QString IS_LAST_ROW_DATA;

    static const QVariant PURPOSE_VIEW_REQUEST;
    static const QVariant PURPOSE_GET_DOCUMENT_BY_ID_REQUEST;
    static const QVariant PURPOSE_VIEW_CREATION;
public:
    static const QString SPECIAL_VIEW_ALL_DOCS;
    explicit CouchDbViewHandler(const QString &viewPath, CouchDbClient *couchDbClient, QObject *parent = 0);
    explicit CouchDbViewHandler(const QString &designDocumentId,
                                const QString &viewName,
                                const QString &mapJavascriptFunctionBody,
                                CouchDbClient *couchDbClient, QObject *parent = 0);
signals:
    /// emited on every row of view after calling run
    /// if includeDocumentBody equals true, document that represents view result
    /// will be emited also, else, document will only contain id field.
    void viewDocumentReady(QVariantMap document,
                           const QVariant &key,
                           const QVariant &value,
                           const cdbqt::CouchDbError::Error &error, const QVariant &userData);

    /// emited after all rows of current view on run were handled
    void noMoreRows(const cdbqt::CouchDbError::Error &error, const QVariant &userData);

    /// used internally
    void get(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariant &userData = QVariant());
    void put(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariantMap &jsonData, const QVariant &userData = QVariant());
public slots:
    /// starts emitting rows results of view
    ///
    /// @includeDocumentBody    if true document of viewDocumentReady, will contain actual document body,
    ///                         else it will contain only document id
    /// @userData               this userData will be emited in viewDocumentReady and in noMoreRows
    void run(bool includeDocumentBody = true, const QVariant &userData = QVariant());

    /// for inner purposes
    void responseDocumentReady(QVariantMap document, const CouchDbError::Error &error, const QVariant &userData);
private:
    CouchDbClient *client;

    bool needToCreateView;
    QString viewPath;
    QString designDocumentId;
    QString viewName;
    QString mapFunctionBody;

    void init();
    
    void runInternal(bool includeDocumentBody = true, const QVariant &userData = QVariant());
    void createView(bool includeDocumentBody = true, const QVariant &userData = QVariant());
};

}

#endif // COUCHDBVIEWHANDLER_H

