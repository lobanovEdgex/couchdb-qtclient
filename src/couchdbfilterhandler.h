#ifndef COUCHDBFILTERHANDLER_H
#define COUCHDBFILTERHANDLER_H

#include "couchdbresponsereceiver.h"

namespace cdbqt {

class CouchDbClient;

/** Class for handling couchdb filters.
 * Current version can create filters in database.
 */
class CouchDbFilterHandler : public CouchDbResponseReceiver
{
    Q_OBJECT
    static const QString PURPOSE_DATA;

    static const QVariant PURPOSE_FILTER_CREATION;
public:
    explicit CouchDbFilterHandler(const QString &filterPath, CouchDbClient *couchDbClient, QObject *parent = 0);
    explicit CouchDbFilterHandler(const QString &designDocumentId,
                                const QString &filterName,
                                const QString &filterJavascriptFunctionBody,
                                CouchDbClient *couchDbClient, QObject *parent = 0);
signals:
    /// emited after filter creation finished or failed
    void filterCreated(const cdbqt::CouchDbError::Error &error);

    /// used internally
    void put(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariantMap &jsonData, const QVariant &userData = QVariant());
public slots:
    void create();

    /// for inner purposes
    void responseDocumentReady(QVariantMap document, const CouchDbError::Error &error, const QVariant &userData);
private:
    CouchDbClient *client;

    bool needToCreateFilter;
    QString filterPath;
    QString designDocumentId;
    QString filterName;
    QString filterFunctionBody;

    void init();
    void createFilter();
};

}

#endif // COUCHDBFILTERHANDLER_H
