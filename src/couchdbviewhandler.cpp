#include "couchdbviewhandler.h"

#include <Logger.h>

#include "couchdbclient.h"
#include "couchdbdocumenthandler.h"

const QString cdbqt::CouchDbViewHandler::EXTERNAL_USER_DATA = "externalUserData";
const QString cdbqt::CouchDbViewHandler::INCLUDE_DOCUMENT_BODY_DATA = "includeDocumentBodyData";
const QString cdbqt::CouchDbViewHandler::PURPOSE_DATA = "purposeData";
const QString cdbqt::CouchDbViewHandler::KEY_DATA = "keyData";
const QString cdbqt::CouchDbViewHandler::VALUE_DATA = "valueData";
const QString cdbqt::CouchDbViewHandler::IS_LAST_ROW_DATA = "isLastRow";


const QVariant cdbqt::CouchDbViewHandler::PURPOSE_VIEW_REQUEST = QVariant(0);
const QVariant cdbqt::CouchDbViewHandler::PURPOSE_GET_DOCUMENT_BY_ID_REQUEST = QVariant(1);
const QVariant cdbqt::CouchDbViewHandler::PURPOSE_VIEW_CREATION = QVariant(2);

const QString cdbqt::CouchDbViewHandler::SPECIAL_VIEW_ALL_DOCS = "_all_docs";

cdbqt::CouchDbViewHandler::CouchDbViewHandler(const QString &viewPath, cdbqt::CouchDbClient *couchDbClient, QObject *parent)
    : CouchDbResponseReceiver(parent), client(couchDbClient), viewPath(viewPath)
{
    init();
    needToCreateView = false;
}

cdbqt::CouchDbViewHandler::CouchDbViewHandler(const QString &designDocumentId, const QString &viewName, const QString &mapJavascriptFunctionBody, cdbqt::CouchDbClient *couchDbClient, QObject *parent)
     : CouchDbResponseReceiver(parent), client(couchDbClient), designDocumentId(designDocumentId),
       viewName(viewName), mapFunctionBody(mapJavascriptFunctionBody)
{
    init();
    needToCreateView = true;
    viewPath =  QString("%1/_view/%2").arg(designDocumentId).arg(viewName);
}

void cdbqt::CouchDbViewHandler::run(bool includeDocumentBody, const QVariant &userData)
{
    if(needToCreateView == false) {
        runInternal(includeDocumentBody, userData);
    } else {
        createView(includeDocumentBody, userData);
    }
}

void cdbqt::CouchDbViewHandler::responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData)
{
    QVariantMap dataWrapper = userData.toMap();
    QVariant purpose = dataWrapper[PURPOSE_DATA];
    if(purpose == PURPOSE_VIEW_REQUEST) {
        QVariant userData = dataWrapper[EXTERNAL_USER_DATA];
        bool includeDocumentBody = dataWrapper[INCLUDE_DOCUMENT_BODY_DATA].toBool();
        if(error != CouchDbError::NO_ERROR) {
            emit(noMoreRows(error, userData));
        } else {
            if(document.count("rows") == 0) {
                emit(noMoreRows(cdbqt::CouchDbError::CONTENT_NOT_FOUND, userData));
            } else {
                QVariantList rows = document["rows"].toList();
                if(rows.size() == 0) {
                    emit(noMoreRows(cdbqt::CouchDbError::NO_ERROR, userData));
                } else {
                    LOG_INFO() << QString("View at path %1 rows: %2").arg(viewPath).arg(rows.size());
                    for(int i = 0; i < rows.size(); i++) {
                        QVariant rowVariant = rows[i];
                        QVariantMap row = rowVariant.toMap();
                        QString documentId = row["id"].toString();
                        QVariant key = row["key"];
                        QVariant value = row["value"];
                        if(includeDocumentBody == false) {
                            QVariantMap document;
                            document[DOCUMENT_ID_FIELD] = documentId;
                            emit(viewDocumentReady(document, key, value, cdbqt::CouchDbError::NO_ERROR, userData));
                        } else {
                            //need to get document first
                            QVariantMap dataWrapper;
                            dataWrapper[EXTERNAL_USER_DATA] = userData;
                            dataWrapper[PURPOSE_DATA] = PURPOSE_GET_DOCUMENT_BY_ID_REQUEST;
                            dataWrapper[KEY_DATA] = key;
                            dataWrapper[VALUE_DATA] = value;
                            dataWrapper[IS_LAST_ROW_DATA] = (i == (rows.size() - 1));
                            QUrl url = client->getUrlForPathForCurrentDatabase(documentId);
                            emit(get(this, url, QVariant(dataWrapper)));
                        }
                    }
                    if(includeDocumentBody == false) {
                        emit(noMoreRows(cdbqt::CouchDbError::NO_ERROR, userData));
                    }
                }
            }
        }
    } else if(purpose == PURPOSE_GET_DOCUMENT_BY_ID_REQUEST) {
        QVariant userData = dataWrapper[EXTERNAL_USER_DATA];
        QVariant key = dataWrapper[KEY_DATA];
        QVariant value = dataWrapper[VALUE_DATA];
        bool isLastRowData = dataWrapper[IS_LAST_ROW_DATA].toBool();
        emit(viewDocumentReady(document, key, value, error, userData));
        if(isLastRowData) {
            emit(noMoreRows(cdbqt::CouchDbError::NO_ERROR, userData));
        }
    } else if(purpose == PURPOSE_VIEW_CREATION) {
        if(error == CouchDbError::NO_ERROR
                || error == CouchDbError::FILE_EXISTS
                || error == CouchDbError::CONFLICT) {

            QVariant userData = dataWrapper[EXTERNAL_USER_DATA];
            bool includeDocumentBody = dataWrapper[INCLUDE_DOCUMENT_BODY_DATA].toBool();

            needToCreateView = false;

            runInternal(includeDocumentBody, userData);
        } else {
            emit(noMoreRows(error, userData));
        }
    }
}

void cdbqt::CouchDbViewHandler::init()
{
    bool connected = connect(this, SIGNAL(get(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariant)),
                             client, SLOT(get(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariant)));
    Q_ASSERT(connected == true);

    connected = connect(this, SIGNAL(put(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariantMap,QVariant)),
                             client, SLOT(put(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariantMap,QVariant)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbViewHandler::runInternal(bool includeDocumentBody, const QVariant &userData)
{
    QUrl url = client->getUrlForPathForCurrentDatabase(viewPath, false);
    QVariantMap dataWrapper;
    dataWrapper[PURPOSE_DATA] = PURPOSE_VIEW_REQUEST;
    dataWrapper[EXTERNAL_USER_DATA] = userData;
    dataWrapper[INCLUDE_DOCUMENT_BODY_DATA] = includeDocumentBody;

    emit(get(this, url, QVariant(dataWrapper)));
}

void cdbqt::CouchDbViewHandler::createView(bool includeDocumentBody, const QVariant &userData)
{
    QUrl url = client->getUrlForPathForCurrentDatabase(designDocumentId);

    QVariantMap designDocument;
    designDocument = documentWithId(designDocumentId);
    designDocument["language"] = "javascript";
    
    QVariantMap viewMap;
    viewMap["map"] = mapFunctionBody;
    
    QVariantMap viewsMap;
    viewsMap[viewName] = viewMap;

    designDocument["views"] = viewsMap;

    QVariantMap dataWrapper;
    dataWrapper[PURPOSE_DATA] = PURPOSE_VIEW_CREATION;
    dataWrapper[EXTERNAL_USER_DATA] = userData;
    dataWrapper[INCLUDE_DOCUMENT_BODY_DATA] = includeDocumentBody;

    emit(put(this, url, designDocument, QVariant(dataWrapper)));
}
