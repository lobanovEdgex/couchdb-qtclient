#ifndef CDBQTMETATYPEREGISTRATION_H
#define CDBQTMETATYPEREGISTRATION_H

#include <QMetaType>

namespace cdbqt {

template <typename Type> class MetaTypeRegistration
{
public:
    inline MetaTypeRegistration()
    {
        qRegisterMetaType<Type>();
    }
};

}

#endif // CDBQTMETATYPEREGISTRATION_H
