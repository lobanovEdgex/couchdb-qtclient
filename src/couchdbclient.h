#ifndef COUCHDBCLIENT_H
#define COUCHDBCLIENT_H

#include <QObject>
#include <QUrl>
#include "couchdbresponsereceiver.h"
#include "couchdbproperties.h"
#include "couchdbutils.h"
#include "couchdberror.h"

class QUrl;
class QVariant;
class QNetworkAccessManager;
class QNetworkRequest;
class QNetworkReply;

namespace cdbqt {

class CouchDbProperties;

class CouchDbDocumentHandler;
class CouchDbViewHandler;
class CouchDbFilterHandler;
class CouchDbAttachmentHandler;

class CouchDbRawResponseReceiver;

/** Couchdb client class
 *
 * Class handles creation of couchDbHandlers (used for working with documents in
 * couchdb), and work with changesets in database. Contains mechanism for
 * creating nonexistent databases.
 *
 * Usually you should have one object of this class per database.
 */
class CouchDbClient : public CouchDbResponseReceiver
{
    Q_OBJECT
public:

private:
    static const QString ATTACHMENT_FIELD_NAME;
    static const int HEART_BEAT;

    enum RequestType {
        CREATING_DATABASE,
        DATABASE_INFO_DOCUMENT,
        CHANGESET_DOCUMENT
    };
public:
    explicit CouchDbClient(CouchDbProperties *properties, QObject *parent = 0);

    CouchDbProperties *getProperties() const;
    /** Returns url set to couchdb instance scheme://host:port
     * where scheme, host and port set from properties.
     */
    QUrl getBaseUrl() const;
    /** Returns url to couchdb instance for path scheme://host:port/dbName/@path
     * where scheme, host, port and dbName are set from properties.
     * If @encodeSlashes set to true, all '/' symbols in path will be encoded with %2F, else
     * slashes are treated as path dividers.
     */
    QUrl getUrlForPathForCurrentDatabase(const QString &path, bool encodeSlashes = true) const;
    QUrl getUrlForPathWithAttachmentForCurrentDatabase(const QString &path, const QString &attachment) const;
    CouchDbDocumentHandler *createDocumentHandler(QObject *parent = 0);
    CouchDbAttachmentHandler *createAttachmentHandler(QObject *parent = 0);
    CouchDbFilterHandler *createFilterHandler(const QString &filterPath, QObject *parent = 0);
    CouchDbFilterHandler *createFilterHandler(const QString &designDocumentId,
                                          const QString &filterName,
                                          const QString &filterJavascriptFunctionBody,
                                          QObject *parent = 0);

    CouchDbViewHandler *createViewHandler(const QString &viewPath, QObject *parent = 0);
    CouchDbViewHandler *createViewHandler(const QString &designDocumentId,
                                          const QString &viewName,
                                          const QString &mapJavascriptFunctionBody,
                                          QObject *parent = 0);
signals:
    /** Signal emitted after initialization of database finished. If no error
     * has occurred error==NO_ERROR.
     */
    void couchDbClientFinishedInitialization(const cdbqt::CouchDbError::Error &error);
    /** Signal emitted on every changesetDocument received. If an error has occured during
     * receivment, document will be empty.
     */
    void documentChanged(QVariantMap document, const cdbqt::CouchDbError::Error &error);
    /** Used for internal purposes (deleting old reply objects before reinitialize
     * changeset.
     */
    void initChangeset();
public slots:
    /** Must be called after constructor (initialization of database).
     * On finish sends couchDbClientFinishedInitialization();
     */
    void init();
    /** Must be called after initialization finished.
     * Starts listening for changesets in database emiting documentChanged * on every change. Listening for changes starting with current updateSeq.
     */
    void startChangesetListening();

    /** Makes get request on @url. @responseReciever must have responseDocumentReady slot.
     */
    void get(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariant &userData = QVariant());
    /** Makes get request on @url. @rawResponseReciever must have responseDataReady slot.
     */
    void get(const cdbqt::CouchDbRawResponseReceiver *rawResponseReciever, const QUrl &url, const QVariant &userData = QVariant());
    /** Makes put request on @url with data @data. @responseReciever must have responseDocumentReady slot. */
    void put(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QByteArray &data, const QVariant &userData = QVariant());
    void put(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariantMap &jsonData, const QVariant &userData = QVariant());
    /** Makes delete request on @url with data @data. @responseReciever must have responseDocumentReady slot. */
    void deleteResource(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariant &userData = QVariant());
private slots:
    void responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData);
    void readChangeset();
    void changesetReadingFinished();
    void authenticationRequired(QNetworkReply *reply,QAuthenticator *oAuth);
    void initChangesetAfter();
private:
    void initInstance(CouchDbProperties *properties, QObject *parent = 0);
    void createDatabase();
    QNetworkRequest getNetworkRequest(const QUrl &url);
    /// used only internally
    void getDocumentById(const QString &id, const RequestType &requestType);
    void finishInitializationWithError(const cdbqt::CouchDbError::Error &error);

    CouchDbProperties *properties;
    QNetworkAccessManager *networkManager;
    QUrl baseUrl;
    int since;

    bool wasReinitChangesetAlreadyLogged;
    bool databaseInitalized;
};


/** Class used for handling QNetworkRequest responses (as
 * QNetworkReply objects) and emiting ready documents.
 * Lives in the same thread as CouchDbClient for thread safety.
 * Object deletes itself after emiting responseDocumentReady
 * signal through deleteLater().
 */
class ReplyHandler : public QObject {
    Q_OBJECT
public:
    /**
     * @parent       must be couchDbHandler that owns reply
     * @userData     used for distinction of different requests inside one handler,
     *               used for state machines, etc..
     */
    explicit ReplyHandler(const QVariant &userData, QObject *parent = 0);
signals:
    /** signal emited when document was recieved
     *
     * @document    contains document body (or empty if an error occurred)
     * @error       contains error if an error has occurred during document
     *              receiving
     * @userData    contains userData
     */
    void responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error,
                               const QVariant &userData = QVariant());
public slots:
    /** slot for finished signal of network reply (assume that finished
     * always called, even when error occurred)
     */
    void finished();
private:
    QVariant userData;
};

/** Class used for handling QNetworkRequest responses (as
 * QNetworkReply objects) and emiting ready raw data.
 * Lives in the same thread as CouchDbClient for thread safety.
 * Object deletes itself after emiting responseDataReady
 * signal through deleteLater().
 */
class RawReplyHandler : public QObject {
    Q_OBJECT
public:
    /**
     * @parent       must be couchDbHandler that owns reply
     * @userData     used for distinction of different requests inside one handler,
     *               used for state machines, etc..
     */
    explicit RawReplyHandler(const QVariant &userData, QObject *parent = 0);
signals:
    /** signal emited when data was recieved
     *
     * @data        contains data (or empty if an error occurred)
     * @error       contains error if an error has occurred during document
     *              receiving
     * @userData    contains userData
     */
    void responseDataReady(const QByteArray &data, const cdbqt::CouchDbError::Error &error,
                               const QVariant &userData = QVariant());
public slots:
    /** slot for finished signal of network reply (assume that finished
     * always called, even when error occurred)
     */
    void finished();
private:
    QVariant userData;
};

}
#endif // COUCHDBCLIENT_H
