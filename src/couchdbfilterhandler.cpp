#include "couchdbfilterhandler.h"

#include <Logger.h>

#include "couchdbclient.h"

const QString cdbqt::CouchDbFilterHandler::PURPOSE_DATA = "purposeData";
const QVariant cdbqt::CouchDbFilterHandler::PURPOSE_FILTER_CREATION = QVariant(0);

cdbqt::CouchDbFilterHandler::CouchDbFilterHandler(const QString &filterPath, cdbqt::CouchDbClient *couchDbClient, QObject *parent)
    : CouchDbResponseReceiver(parent), client(couchDbClient), filterPath(filterPath)
{
    init();
    needToCreateFilter = false;
}

cdbqt::CouchDbFilterHandler::CouchDbFilterHandler(const QString &designDocumentId, const QString &filterName, const QString &filterJavascriptFunctionBody, cdbqt::CouchDbClient *couchDbClient, QObject *parent)
     : CouchDbResponseReceiver(parent), client(couchDbClient), designDocumentId(designDocumentId),
       filterName(filterName), filterFunctionBody(filterJavascriptFunctionBody)
{
    init();
    needToCreateFilter = true;
    filterPath =  QString("%1/%2").arg(designDocumentId).arg(filterName);
}

void cdbqt::CouchDbFilterHandler::create()
{
    createFilter();
}

void cdbqt::CouchDbFilterHandler::responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData)
{
    Q_UNUSED(document);
    QVariantMap dataWrapper = userData.toMap();
    QVariant purpose = dataWrapper[PURPOSE_DATA];
    if(purpose == PURPOSE_FILTER_CREATION) {
        emit(filterCreated(error));
    }
}

void cdbqt::CouchDbFilterHandler::init()
{
    bool connected = connect(this, SIGNAL(put(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariantMap,QVariant)),
                             client, SLOT(put(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariantMap,QVariant)));
    Q_ASSERT(connected == true);
}


void cdbqt::CouchDbFilterHandler::createFilter()
{
    QUrl url = client->getUrlForPathForCurrentDatabase(designDocumentId);

    QVariantMap designDocument;
    designDocument = documentWithId(designDocumentId);
    designDocument["language"] = "javascript";

    QVariantMap filtersMap;
    filtersMap[filterName] = filterFunctionBody;

    designDocument["filters"] = filtersMap;

    QVariantMap dataWrapper;
    dataWrapper[PURPOSE_DATA] = PURPOSE_FILTER_CREATION;

    emit(put(this, url, designDocument, QVariant(dataWrapper)));
}
