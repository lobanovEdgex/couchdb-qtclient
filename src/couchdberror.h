#ifndef COUCHDBERROR_H
#define COUCHDBERROR_H

#include <QObject>
#include <QNetworkReply>
#include <QVariant>
#include "utils/cdbqtmetatyperegistration.h"

namespace cdbqt {

class CouchDbError : public QObject
{
    Q_OBJECT
    Q_ENUMS(Error)
public:
    enum Error {
        NO_ERROR = 0,
        COULD_NOT_PARSE_JSON = -1,
        FILE_EXISTS = -2,
        CONFLICT = -3,
        COULD_NOT_CREATE_DATABASE = -4,
        CONNECTION_REFUSED = 1,
        REMOTE_HOST_CLOSED = 2,
        HOST_NOT_FOUND = 3,
        TIMEOUT_ERROR = 4,
        CONTENT_ACCESS_DENIED = 201,
        CONTENT_OPERATION_NOT_PERMITTED = 202,
        CONTENT_NOT_FOUND = 203,
        AUTHENTICATION_REQUIRED = 204,
        UNKNOWN_ERROR
    };
private:
    explicit CouchDbError(QObject *parent = 0);
public:
    static QString errorToString(const Error &error);
    static Error networkReplyErrorToError(const QNetworkReply::NetworkError &error, const QVariantMap &response);
};

}

static cdbqt::MetaTypeRegistration<cdbqt::CouchDbError::Error> metatypeCouchDbClientError;
Q_DECLARE_METATYPE(cdbqt::CouchDbError::Error)

#endif // COUCHDBERROR_H
