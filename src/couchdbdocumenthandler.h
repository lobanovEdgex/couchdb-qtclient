#ifndef COUCHDBDOCUMENTHANDLER_H
#define COUCHDBDOCUMENTHANDLER_H

#include "couchdbresponsereceiver.h"
#include "couchdberror.h"
#include <QQueue>

namespace cdbqt {

struct UpdatingRequest;

class CouchDbClient;

/** Class for handling requests related to documents.
 * every request ends with emiting "responseReady" or "attachmentReady" signal.
 */
class CouchDbDocumentHandler : public CouchDbResponseReceiver
{
    Q_OBJECT
    static const QString EXTERNAL_USER_DATA;
    static const QString INTERNAL_USER_DATA;
    static const QString DOCUMENT_ID_DATA;
    static const QString ATTACHMENT_PATH_DATA;

    static const int DEFAULT_REVISIONS_EXPIRATION_TIME_FOR_CLEANUP;
    static const int DEFAULT_INTERVAL_BETWEEN_CLEANUPS;

    static const QVariant GET_DOCUMENT_BY_ID_EXTERNAL;
    static const QVariant GET_DOCUMENT_BY_ID_INTERNAL;
    static const QVariant PUT_DOCUMENT_INTERNAL;
    static const QVariant DELETE_ATTACHMENT_INTERNAL;
public:
    CouchDbDocumentHandler(CouchDbClient *client, QObject *parent = 0,
                           int intervalBetweenCleanups = DEFAULT_INTERVAL_BETWEEN_CLEANUPS,
                           int revisionsExpirationTimeForCleanup = DEFAULT_REVISIONS_EXPIRATION_TIME_FOR_CLEANUP);

    ///Mostly for test purposes
    int currentTrackedDocumentsCount() const;
signals:
    ///emmited on retreiving and updating documents
    ///if error has occurred document will contain current tracked document fields on update,
    ///and documentId field on getDocumentById
    void responseReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData);

    /// Used internally
    void get(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariant &userData = QVariant());
    void put(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariantMap &jsonData, const QVariant &userData = QVariant());
    void deleteResource(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariant &userData = QVariant());
public slots:
    /** Function for retrieving documents by it's id.
     * @documentId              id of document for retrieving;
     * @includeAttachments      if true retrieved document will contains data field
     *                          in every attachment with base64 encoded data;
     * @userData                userData that will be emited in the signal
     *                          (for distinguishing requests inside user class).
     */
    void getDocumentWithId(const QString &documentId, bool includeAttachments = false,
                           const QVariant &userData = QVariant());

    /** Function for creating/updating documents or only some of their fields.
     * @documentFields must contain _id field that represents document id.
     * If document with corresponding id was not presented in database, it will be
     * created with fields contained in @documentFields. If @documentFields contains
     * _rev field then request for update will be sent with this revision,
     * else current revision of document will be retrieved first.
     * @forceMerging        if true, document will be updated regardless
     *                      current revision, else will emit error on revisions conflict.
     *
     * @clearOldFields      if true new document revision will contain only fields
     *                      that are inside of @documentFields, else fields from
     *                      previous revision that are not in @documentFields
     *                      will be saved with current values.
     *
     * @userData            userData that will be emited in the signal
     *                      (for distinguishing requests inside user class).
     */
    void updateDocumentFields(const QVariantMap &documentFields,
                              bool clearOldFields = false,
                              bool forceMerging = false,
                              const QVariant &userData = QVariant());

    void deleteAttachment(const QString &documentId, const QString &pathToAttachment,
                           const QVariant &userData = QVariant());
public slots:
    void responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData);
protected:
    CouchDbClient *client;

    /// every processed request increases currentSequence by 1 (after being popped
    /// from queue (which means request has been handled))
    int currentSequence;
    /// cleanup called every intervalBetweenCleanups requests
    int intervalBetweenCleanups;
    /// increases after every request being fully processed
    int totalRequestsProcessed;
    int revisionsExpirationTimeForCleanup;
    /// contains current revisions of documents to prevent obtaining revision
    /// request every time. When currentDocuments.keys() contains some id,
    /// that means document with this id is tracked.
    QMap<QString, QVariantMap> trackedDocuments;
    /// When cleanup called removes all documents from tracking, which has
    /// lastSequence < currentSequence - REVISIONS_EXPIRATION_TIME_FOR_CLEANUP
    /// if processingQueue is empty.
    QMap<QString, int> trackedDocumentsLastSequences;
    /// Contains queue of updating reuqests to be processed.
    QMap<QString, QQueue<UpdatingRequest> > processingQueues;

    void processQueueForDocumentWithId(const QString &documentId);
    void getDocumentWithIdInternal(const QString &documentId,
                           const QVariant &userData = QVariant());

    QVariantMap applyDocumentFields(const QVariantMap &document, const QVariantMap &updateFields, const bool clearOldFields);
    void finishRequestProcessingForDocument(const QString &documentId);
    void cleanup();
};

class UpdatingRequest
{
public:
    static const int UPDATING_REQUEST_TYPE_DELETE_ATTACHMENT;
    static const int UPDATING_REQUEST_TYPE_UPDATE_DOCUMENT_FIELDS;

    UpdatingRequest(const QString &pathToAttachment,
                    const QVariant &userData);

    UpdatingRequest(const QVariantMap &documentFields,
                    const bool clearOldFields,
                    const bool forceMerging,
                    const QVariant &userData);
    //remove attachment
    QString pathToAttachment;
    //update documentFields
    QVariantMap documentFields;
    bool clearOldFields;
    bool forceMerging;
    //common
    QVariant userData;

    int getType();
private:
    int type;
};

}


#endif // COUCHDBDOCUMENTHANDLER_H
