#include "couchdbdocumenthandler.h"

#include <Logger.h>

#include "couchdbclient.h"

const QString cdbqt::CouchDbDocumentHandler::EXTERNAL_USER_DATA = "externalUserData";
const QString cdbqt::CouchDbDocumentHandler::INTERNAL_USER_DATA = "internalUserData";
const QString cdbqt::CouchDbDocumentHandler::DOCUMENT_ID_DATA = "documentIdData";
const QString cdbqt::CouchDbDocumentHandler::ATTACHMENT_PATH_DATA = "attachmentPathData";

const int cdbqt::CouchDbDocumentHandler::DEFAULT_INTERVAL_BETWEEN_CLEANUPS = 100;
const int cdbqt::CouchDbDocumentHandler::DEFAULT_REVISIONS_EXPIRATION_TIME_FOR_CLEANUP = 20;

const QVariant cdbqt::CouchDbDocumentHandler::GET_DOCUMENT_BY_ID_EXTERNAL = QVariant(0);
const QVariant cdbqt::CouchDbDocumentHandler::GET_DOCUMENT_BY_ID_INTERNAL = QVariant(1);
const QVariant cdbqt::CouchDbDocumentHandler::PUT_DOCUMENT_INTERNAL = QVariant(2);
const QVariant cdbqt::CouchDbDocumentHandler::DELETE_ATTACHMENT_INTERNAL = QVariant(3);

cdbqt::CouchDbDocumentHandler::CouchDbDocumentHandler(cdbqt::CouchDbClient *client, QObject *parent,
                                                      int intervalBetweenCleanups, int revisionsExpirationTimeForCleanup) :
    CouchDbResponseReceiver(parent), client(client), intervalBetweenCleanups(intervalBetweenCleanups),
    revisionsExpirationTimeForCleanup(revisionsExpirationTimeForCleanup)
{
    bool connected = connect(this, SIGNAL(get(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariant)),
                             client, SLOT(get(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariant)));
    Q_ASSERT(connected == true);

    connected = connect(this, SIGNAL(put(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariantMap,QVariant)),
                             client, SLOT(put(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariantMap,QVariant)));
    Q_ASSERT(connected == true);

    connected = connect(this, SIGNAL(deleteResource(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariant)),
                             client, SLOT(deleteResource(const cdbqt::CouchDbResponseReceiver*,QUrl,QVariant)));
    Q_ASSERT(connected == true);

    currentSequence = 0;
    totalRequestsProcessed = 0;
}

int cdbqt::CouchDbDocumentHandler::currentTrackedDocumentsCount() const
{
    return trackedDocuments.size();
}

void cdbqt::CouchDbDocumentHandler::getDocumentWithId(const QString &documentId,
                                                      bool includeAttachments, const QVariant &userData)
{
    QUrl url = client->getUrlForPathForCurrentDatabase(documentId);
    url.addEncodedQueryItem("attachments", includeAttachments ? "true" : "false");
    QVariantMap userDataWrapper;
    userDataWrapper[EXTERNAL_USER_DATA] = userData;
    userDataWrapper[INTERNAL_USER_DATA] = GET_DOCUMENT_BY_ID_EXTERNAL;
    userDataWrapper[DOCUMENT_ID_DATA] = documentId;

    emit(get(this, url, QVariant(userDataWrapper)));
}

void cdbqt::CouchDbDocumentHandler::updateDocumentFields(const QVariantMap &documentFields,
                                                         bool clearOldFields, bool forceMerging,
                                                         const QVariant &userData)
{
    if(!documentFields.contains(DOCUMENT_ID_FIELD)) {
        LOG_ERROR() << "document doesn't contain id field";
        return;
    }
    QString documentId = documentFields[DOCUMENT_ID_FIELD].toString();
    if(trackedDocuments.contains(documentId)) {
        QQueue<UpdatingRequest> &queue = processingQueues[documentId];
        //if forceMerging true -> same behavior as documentFields would not contain revision
        if(documentFields.contains(DOCUMENT_REVISION_FIELD) && forceMerging) {
            QVariantMap fieldsWithoutRevision = documentFields;
            fieldsWithoutRevision.remove(DOCUMENT_REVISION_FIELD);
            fieldsWithoutRevision.remove(ATTACHMENTS_FIELD);

            queue.push_back(UpdatingRequest(fieldsWithoutRevision, clearOldFields, forceMerging, userData));
        } else {
            queue.push_back(UpdatingRequest(documentFields, clearOldFields, forceMerging, userData));
        }
        trackedDocumentsLastSequences[documentId] = currentSequence++;
        if(queue.size() == 1) {
            // if queue was empty we must start processing chain
            // else it will be processed after finishing of last request processing.
            processQueueForDocumentWithId(documentId);
        }
    } else {
        trackedDocuments[documentId] = documentFields;
        trackedDocumentsLastSequences[documentId] = currentSequence++;

        processingQueues[documentId] = QQueue<UpdatingRequest>();

        QQueue<UpdatingRequest> &queue = processingQueues[documentId];
        queue.push_back(UpdatingRequest(documentFields, clearOldFields, forceMerging, userData));

        if(documentFields.contains(DOCUMENT_REVISION_FIELD)) { //won't try to obtain current revision, trying to update right away
            processQueueForDocumentWithId(documentId);
        } else {                                                //try to obtain current revision (if document doesn't exist will create it after)
            getDocumentWithIdInternal(documentId, GET_DOCUMENT_BY_ID_INTERNAL);
        }
    }
}

void cdbqt::CouchDbDocumentHandler::deleteAttachment(const QString &documentId, const QString &pathToAttachment, const QVariant &userData)
{
    if(trackedDocuments.contains(documentId)) {
        // if we already track document with documentId we don't need to obtain current revision to delete attachment
        // (we'll use tracked one)
        QQueue<UpdatingRequest> &queue = processingQueues[documentId];
        queue.push_back(UpdatingRequest(pathToAttachment, userData));
        trackedDocumentsLastSequences[documentId] = currentSequence++;
        if(queue.size() == 1) {
            // if queue was empty we must start processing chain
            // else it will be processed after finishing of last request processing.
            processQueueForDocumentWithId(documentId);
        }
    } else {
        QVariantMap documentFields;
        documentFields[DOCUMENT_ID_FIELD] = documentId;
        trackedDocuments[documentId] = documentFields;
        trackedDocumentsLastSequences[documentId] = currentSequence++;

        processingQueues[documentId] = QQueue<UpdatingRequest>();

        QQueue<UpdatingRequest> &queue = processingQueues[documentId];
        queue.push_back(UpdatingRequest(pathToAttachment, userData));

        // try to obtain current revision (if document doesn't exist will create it after)
        getDocumentWithIdInternal(documentId, GET_DOCUMENT_BY_ID_INTERNAL);
    }
}

void cdbqt::CouchDbDocumentHandler::responseDocumentReady(QVariantMap document,
                                                          const cdbqt::CouchDbError::Error &error,
                                                          const QVariant &userData)
{
    QVariantMap userDataWrapper = userData.toMap();
    QVariant internalUserData = userDataWrapper[INTERNAL_USER_DATA].toString();
    if(internalUserData == GET_DOCUMENT_BY_ID_EXTERNAL) {
        QString documentId = userDataWrapper[DOCUMENT_ID_DATA].toString();
        if(error == cdbqt::CouchDbError::NO_ERROR) {
            emit(responseReady(document, error, userDataWrapper[EXTERNAL_USER_DATA]));
        } else {
            document[DOCUMENT_ID_FIELD] = documentId;
            emit(responseReady(document, error, userDataWrapper[EXTERNAL_USER_DATA]));
        }
    } else if(internalUserData == GET_DOCUMENT_BY_ID_INTERNAL) {
        QString documentId = userDataWrapper[DOCUMENT_ID_DATA].toString();
        if(error == cdbqt::CouchDbError::NO_ERROR) {
            //obtained last revision, update tracked document
            //and can process queue
            trackedDocuments[documentId] = document;

            processQueueForDocumentWithId(documentId);
        } else if(error == cdbqt::CouchDbError::CONTENT_NOT_FOUND) {
            //database doesn't contain document we'll create it by processing request
            processQueueForDocumentWithId(documentId);
        } else {
            //unexpectable error has occured, can't process request any further
            //error will occur on processQueuedDocument
            LOG_ERROR() << "Unexpectable error while obtaining document revision" << CouchDbError::errorToString(error);
            processQueueForDocumentWithId(documentId);
        }

    } else if(internalUserData == PUT_DOCUMENT_INTERNAL) {
        QString documentId = userDataWrapper[DOCUMENT_ID_DATA].toString();
        Q_ASSERT(!processingQueues[documentId].isEmpty());
        QQueue<UpdatingRequest> &queue = processingQueues[documentId];
        UpdatingRequest updatingRequest = queue.front();
        QVariantMap oldDocument = trackedDocuments[documentId];
        QVariant &userData = updatingRequest.userData;
        if(error == CouchDbError::NO_ERROR) {
            //everything okay, need to update document and it's revision
            QVariantMap newDocument = applyDocumentFields(oldDocument,
                                                          updatingRequest.documentFields,
                                                          updatingRequest.clearOldFields);
            //apply new revision (contained in result of put document request)
            newDocument[DOCUMENT_REVISION_FIELD] = document["rev"].toString();
            //update request is over, pop it from request queue
            trackedDocuments[documentId] = newDocument;
            finishRequestProcessingForDocument(documentId);
            emit(responseReady(newDocument, error, userData));
        } else if(error == CouchDbError::CONFLICT) {
            //if forceMergeConflits or if update revision was not specified, but current
            //outdated, try to obtain latest revision, and processs again,
            //else emit error for this update request
            if(updatingRequest.forceMerging
                    || updatingRequest.documentFields.contains(DOCUMENT_REVISION_FIELD) == false) {
                //processing will take place after receiving newest revision
                getDocumentWithIdInternal(documentId, GET_DOCUMENT_BY_ID_INTERNAL);
            } else {
                LOG_WARNING() << "merge conflict on document with id" << documentId;
                //dismiss update request
                finishRequestProcessingForDocument(documentId);
                emit(responseReady(oldDocument, error, userData));
            }
        } else {
            //unexpected error, emit error
            LOG_ERROR() << "unexpected error" << CouchDbError::errorToString(error) << "on document with id" << documentId;
            //dismiss update request
            finishRequestProcessingForDocument(documentId);
            emit(responseReady(oldDocument, error, userData));
        }
    } else if(internalUserData == DELETE_ATTACHMENT_INTERNAL) {
        QString documentId = userDataWrapper[DOCUMENT_ID_DATA].toString();
        QString pathToAttachment = userDataWrapper[ATTACHMENT_PATH_DATA].toString();
        Q_ASSERT(!processingQueues[documentId].isEmpty());
        QQueue<UpdatingRequest> &queue = processingQueues[documentId];
        UpdatingRequest updatingRequest = queue.front();
        QVariantMap oldDocument = trackedDocuments[documentId];
        QVariant &userData = updatingRequest.userData;
        if(error == CouchDbError::NO_ERROR) {
            //everything okay, need to update document's revision
            QVariantMap newDocument = oldDocument;
            //apply new revision (contained in result of put document request)
            newDocument[DOCUMENT_REVISION_FIELD] = document["rev"].toString();
            //remove attachment from _attachment field
            QVariantMap attachmentsField = newDocument[ATTACHMENTS_FIELD].toMap();
            attachmentsField.remove(pathToAttachment);
            if(attachmentsField.size() == 0) {
                newDocument.remove(ATTACHMENTS_FIELD);
            } else {
                newDocument[ATTACHMENTS_FIELD] = attachmentsField;
            }

            //update request is over, pop it from request queue
            trackedDocuments[documentId] = newDocument;
            finishRequestProcessingForDocument(documentId);
            emit(responseReady(newDocument, error, userData));
        } else if(error == CouchDbError::CONFLICT) {
            //as we not specify revision on deliting document attachment
            //we try to obtain latest revision, and processs again,
            //else emit error for this update request
            getDocumentWithIdInternal(documentId, GET_DOCUMENT_BY_ID_INTERNAL);

            //processing will take place after receiving newest revision
        } else {
            //unexpected error, emit error
            LOG_ERROR() << "unexpected error" << CouchDbError::errorToString(error) << "on document with id" << documentId;
            //dismiss update request
            finishRequestProcessingForDocument(documentId);
            emit(responseReady(oldDocument, error, userData));
        }
    }
}

void cdbqt::CouchDbDocumentHandler::processQueueForDocumentWithId(const QString &documentId)
{
    Q_ASSERT(processingQueues.contains(documentId));

    QQueue<UpdatingRequest> &queue = processingQueues[documentId];
    if(queue.isEmpty()) {
    } else {
        QVariantMap currentDocument = trackedDocuments[documentId];
        UpdatingRequest &updatingRequest = queue.front();
        if(updatingRequest.getType() == UpdatingRequest::UPDATING_REQUEST_TYPE_UPDATE_DOCUMENT_FIELDS) {
            QVariantMap newDocument = applyDocumentFields(currentDocument, updatingRequest.documentFields, updatingRequest.clearOldFields);

            QUrl url = client->getUrlForPathForCurrentDatabase(documentId);
            QVariantMap userDataWrapper;
            userDataWrapper[INTERNAL_USER_DATA] = PUT_DOCUMENT_INTERNAL;
            userDataWrapper[DOCUMENT_ID_DATA] = documentId;

            emit(put(this, url, newDocument, QVariant(userDataWrapper)));
        } else if(updatingRequest.getType() == UpdatingRequest::UPDATING_REQUEST_TYPE_DELETE_ATTACHMENT) {
            QUrl url = client->getUrlForPathWithAttachmentForCurrentDatabase(documentId, updatingRequest.pathToAttachment);
            url.addEncodedQueryItem("rev", QUrl::toPercentEncoding(currentDocument[DOCUMENT_REVISION_FIELD].toString()));

            QVariantMap userDataWrapper;
            userDataWrapper[INTERNAL_USER_DATA] = DELETE_ATTACHMENT_INTERNAL;
            userDataWrapper[DOCUMENT_ID_DATA] = documentId;
            userDataWrapper[ATTACHMENT_PATH_DATA] = updatingRequest.pathToAttachment;

            emit(deleteResource(this, url, QVariant(userDataWrapper)));
        } else {
            Q_ASSERT(false);
        }
    }
}

void cdbqt::CouchDbDocumentHandler::getDocumentWithIdInternal(const QString &documentId, const QVariant &userData)
{
    QVariantMap userDataWrapper;
    userDataWrapper[INTERNAL_USER_DATA] = userData;
    userDataWrapper[DOCUMENT_ID_DATA] = documentId;

    QUrl url = client->getUrlForPathForCurrentDatabase(documentId);
    emit(get(this, url, QVariant(userDataWrapper)));
}

QVariantMap cdbqt::CouchDbDocumentHandler::applyDocumentFields(const QVariantMap &document, const QVariantMap &updateFields, const bool clearOldFields)
{
    QVariantMap result;
    if(clearOldFields == false) {
       result = document;
    } else {
        result[DOCUMENT_ID_FIELD] = document[DOCUMENT_ID_FIELD];
        if(document.contains(DOCUMENT_REVISION_FIELD)) {
            result[DOCUMENT_REVISION_FIELD] = document[DOCUMENT_REVISION_FIELD];
        }
    }
    foreach (QString key, updateFields.keys()) {
        result[key] = updateFields.value(key);
    }
    return result;
}

void cdbqt::CouchDbDocumentHandler::finishRequestProcessingForDocument(const QString &documentId)
{
    processingQueues[documentId].pop_front();
    totalRequestsProcessed++;
    if(totalRequestsProcessed != 0 && totalRequestsProcessed % intervalBetweenCleanups == 0) {
        cleanup();
    }
    //we might just deleted it, so check
    if(processingQueues.contains(documentId)) {
        processQueueForDocumentWithId(documentId);
    }
}

///TODO add set for documents with empty processingQueue (for performance)
void cdbqt::CouchDbDocumentHandler::cleanup()
{
    QSet<QString> toBeDeleted;
    foreach(QString trackedDocumentId, trackedDocuments.keys()) {
        Q_ASSERT(processingQueues.count(trackedDocumentId) != 0);
        //cleanup only when all requests were processed
        if(processingQueues[trackedDocumentId].size() == 0) {
            Q_ASSERT(trackedDocumentsLastSequences.count(trackedDocumentId) != 0);
            int documentLastSequence = trackedDocumentsLastSequences[trackedDocumentId];
            if(documentLastSequence < currentSequence - revisionsExpirationTimeForCleanup) {
                //perform cleanup
                toBeDeleted.insert(trackedDocumentId);
            }
        }
    }
    if(toBeDeleted.size() != 0) {
        LOG_INFO() << "Cleanup removes" << toBeDeleted.size() << "documents from tracking" ;
    }
    foreach(QString trackedDocumentId, toBeDeleted) {
        trackedDocuments.remove(trackedDocumentId);
        trackedDocumentsLastSequences.remove(trackedDocumentId);
        processingQueues.remove(trackedDocumentId);
    }
}


const int cdbqt::UpdatingRequest::UPDATING_REQUEST_TYPE_UPDATE_DOCUMENT_FIELDS = 0;
const int cdbqt::UpdatingRequest::UPDATING_REQUEST_TYPE_DELETE_ATTACHMENT = 1;

cdbqt::UpdatingRequest::UpdatingRequest(const QString &pathToAttachment, const QVariant &userData) :
    pathToAttachment(pathToAttachment),
    userData(userData),
    type(UPDATING_REQUEST_TYPE_DELETE_ATTACHMENT)
{
}

cdbqt::UpdatingRequest::UpdatingRequest(const QVariantMap &documentFields,
                                 const bool clearOldFields, const bool forceMerging, const QVariant &userData) :
    documentFields(documentFields),
    clearOldFields(clearOldFields),
    forceMerging(forceMerging),
    userData(userData),
    type(UPDATING_REQUEST_TYPE_UPDATE_DOCUMENT_FIELDS)
{
}

int cdbqt::UpdatingRequest::getType()
{
    return this->type;
}
