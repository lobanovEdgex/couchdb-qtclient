#ifndef COUCHDBPROPERTIES_H
#define COUCHDBPROPERTIES_H

#include <QObject>

namespace cdbqt {

class CouchDbProperties : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString dbName READ dbName)
    Q_PROPERTY(QString protocol READ protocol)
    Q_PROPERTY(QString host READ host)
    Q_PROPERTY(int port READ port)
    Q_PROPERTY(QString username READ username)
    Q_PROPERTY(QString password READ password)

public:
    explicit CouchDbProperties(const QString &dbName,
                               QString protocol = "http",
                               QString host = "127.0.0.1",
                               int port = 5984,
                               QString username = QString(),
                               QString password = QString(),
                               QObject *parent = 0);

    explicit CouchDbProperties(const QString &dbName,
                               QObject *parent,
                               QString protocol = "http",
                               QString host = "127.0.0.1",
                               int port = 5984,
                               QString username = QString(),
                               QString password = QString());

    QString dbName() const;
    QString protocol() const;
    QString host() const;
    int port() const;
    QString username() const;
    QString password() const;
private:

    QString m_dbName;
    QString m_protocol;
    QString m_host;
    int m_port;
    QString m_username;
    QString m_password;

signals:

public slots:

};

}

#endif // COUCHDBPROPERTIES_H
