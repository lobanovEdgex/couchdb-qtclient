#include "couchdberror.h"

cdbqt::CouchDbError::CouchDbError(QObject *parent) :
    QObject(parent)
{

}

QString cdbqt::CouchDbError::errorToString(const cdbqt::CouchDbError::Error &error)
{
    switch(error) {
    case NO_ERROR:
        return "NO ERROR";
    case COULD_NOT_PARSE_JSON:
        return "Could not parse json of reply";
    case COULD_NOT_CREATE_DATABASE:
        return "Could not create database";
    case FILE_EXISTS:
        return "File exists error";
    case CONTENT_NOT_FOUND:
        return "Content not found";
    case CONNECTION_REFUSED:
        return "Connection refused";
    case REMOTE_HOST_CLOSED:
        return "Remote host closed";
    case HOST_NOT_FOUND:
        return "Host not found";
    case TIMEOUT_ERROR:
        return "Connection timed out";
    case CONTENT_ACCESS_DENIED:
        return "Content access denied";
    case CONTENT_OPERATION_NOT_PERMITTED:
        return "Content operation not permitted";
    case AUTHENTICATION_REQUIRED:
        return "Authentication required";
    default:
        return "CAN'T CONVERT ERROR TO STRING";
    }
    return "CAN'T CONVERT ERROR TO STRING";
}

cdbqt::CouchDbError::Error cdbqt::CouchDbError::networkReplyErrorToError(const QNetworkReply::NetworkError &error, const QVariantMap &response)
{
    QString errorString = response["error"].toString();
    switch(error) {
    case QNetworkReply::NoError:
        return NO_ERROR;
    case QNetworkReply::ConnectionRefusedError:
        return CONNECTION_REFUSED;
    case QNetworkReply::RemoteHostClosedError:
        return REMOTE_HOST_CLOSED;
    case QNetworkReply::HostNotFoundError:
        return HOST_NOT_FOUND;
    case QNetworkReply::TimeoutError:
        return TIMEOUT_ERROR;
    case QNetworkReply::ContentAccessDenied:
        return CONTENT_ACCESS_DENIED;
    case QNetworkReply::ContentOperationNotPermittedError:
        return CONTENT_OPERATION_NOT_PERMITTED;
    case QNetworkReply::ContentNotFoundError:
        return CONTENT_NOT_FOUND;
    case QNetworkReply::AuthenticationRequiredError:
        return AUTHENTICATION_REQUIRED;
    case QNetworkReply::UnknownContentError:
        if(errorString == "file_exists") {
            return FILE_EXISTS;
        } else if(errorString == "conflict") {
            return CONFLICT;
        } else if(errorString == "missing_stub") {
            return CONTENT_NOT_FOUND;
        }
    default:
        return UNKNOWN_ERROR;
    }
    return UNKNOWN_ERROR;
}
