#ifndef COUCHDBRAWRESPONSERECEIVER_H
#define COUCHDBRAWRESPONSERECEIVER_H

#include <QObject>

#include "couchdberror.h"

namespace cdbqt {

/** Class defines abstract slot for hanling raw data responses from couchdbClient
 */
class CouchDbRawResponseReceiver : public QObject
{
    Q_OBJECT
public:
    explicit CouchDbRawResponseReceiver(QObject *parent = 0);
public slots:
    virtual void responseDataReady(const QByteArray &data, const cdbqt::CouchDbError::Error &error, const QVariant &userData) = 0;

};

}

#endif // COUCHDBRAWRESPONSERECEIVER_H
