#ifndef COUCHDBATTACHMENTHANDLER_H
#define COUCHDBATTACHMENTHANDLER_H

#include "couchdbrawresponsereceiver.h"
#include "couchdberror.h"
#include <QQueue>
#include <QList>
#include <QMap>
#include <QPair>
#include "utils/cdbqtmetatyperegistration.h"


namespace cdbqt {

struct UpdatingRequest;

class CouchDbClient;

/** Class for handling requests related to documents.
 * every request ends with emiting "responseReady" or "attachmentReady" signal.
 */

class AttachmentRequestsGroup;

typedef QMap<QString, QByteArray> AttachmentDataResponse;

typedef QMap<QString,  CouchDbError::Error> AttachmentErrorResponse;

class CouchDbAttachmentHandler : public CouchDbRawResponseReceiver
{
    static const QString EXTERNAL_USER_DATA;
    static const QString INTERNAL_USER_DATA;
    static const QString DOCUMENT_ID_DATA;
    static const QString ATTACHMENT_PATH_DATA;
    static const QString ATTACHMENT_REQUEST_GROUP_DATA;

    static const QVariant SINGLE_ATTACHMENT_REQUEST;
    static const QVariant ATTACHMENTS_REQUEST;
    Q_OBJECT
public:
    CouchDbAttachmentHandler(CouchDbClient *client, QObject *parent = 0);
signals:
    ///emmited on retreiving attachment
    ///if error has occurred document will contain current tracked document fields and data will be empty
    void attachmentReady(QString documentId, const QString &pathToAttachment,
                         const QByteArray &data, const cdbqt::CouchDbError::Error &error, const QVariant &userData);

    ///emmited on retreiving attachment
    ///if error has occurred document will contain current tracked document fields and data will errors map contains errors different from NO_ERROR
    ///therefore to check if there was an error during request we must check errors.size() != 0
    void attachmentsReady(QString documentId, const cdbqt::AttachmentDataResponse &attachmentsData,
                          const cdbqt::AttachmentErrorResponse &errors, const QVariant &userData);

    /// Used internally
    void get(const cdbqt::CouchDbRawResponseReceiver *rawResponseReciever, const QUrl &url, const QVariant &userData = QVariant());

public slots:
    /** Function for retrieving attachment by document and attachment name.
     * @documentId              id of document that conatains attachment;
     *
     * @attachmentPath          path to attachment inside document
     * @userData                userData that will be emited in the signal
     *                          (for distinguishing requests inside user class).
     */
    void getAttachment(const QString &documentId, const QString &attachmentPath,
                           const QVariant &userData = QVariant());

    /** Function for retrieving attachments by document and attachments name.
     * @documentId              id of document that conatains attachment;
     *
     * @attachmentsPaths        list of attachments paths inside document to retrieve
     * @userData                userData that will be emited in the signal
     *                          (for distinguishing requests inside user class).
     */
    void getAttachments(const QString &documentId, const QList<QString> &attachmentsPaths,
                           const QVariant &userData = QVariant());
protected:
    CouchDbClient *client;
    // CouchDbRawResponseReceiver interface
public slots:
    void responseDataReady(const QByteArray &data, const CouchDbError::Error &error, const QVariant &userData);
};

class AttachmentRequestsGroup : public QObject
{
    Q_OBJECT
public:
    explicit AttachmentRequestsGroup(const QList<QString> &attachmentsPaths, QObject *parent = 0);
    explicit AttachmentRequestsGroup(QObject *parent = 0);
    bool allRequestsFinished();
    void addAttachment(const QString &path, const QByteArray &data, const CouchDbError::Error &error);
    QMap<QString, CouchDbError::Error> getResponseErrors(); //attachment path -> Error
    QMap<QString, QByteArray> getResponseData();
private:
    QList<QString> attachmentsPaths;
    QMap<QString, QByteArray> responseData; //attachment path -> data
    QMap<QString, CouchDbError::Error> responseErrors; //attachment path -> Error
};

} //namespace cdbqt

static cdbqt::MetaTypeRegistration<cdbqt::AttachmentDataResponse> metatypeAttachmentDataResponse;
Q_DECLARE_METATYPE(cdbqt::AttachmentDataResponse)

static cdbqt::MetaTypeRegistration<cdbqt::AttachmentErrorResponse> metatypeAttachmentErrorResponse;
Q_DECLARE_METATYPE(cdbqt::AttachmentErrorResponse)
#endif // COUCHDBATTACHMENTHANDLER_H
