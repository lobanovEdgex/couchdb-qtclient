#include "couchdbclient.h"

#include <QObject>
#include <QUrl>
#include <qjson/parser.h>
#include <qjson/serializer.h>
#if QT_VERSION >= 0x050000
    #include <QtNetwork/QNetworkAccessManager>
    #include <QtNetwork/QNetworkRequest>
    #include <QtNetwork/QNetworkReply>
#else
    #include <QNetworkAccessManager>
    #include <QNetworkRequest>
    #include <QNetworkReply>
#endif

#include <QAuthenticator>
#include <QCoreApplication>

#include <QVariant>
#include <QVariantMap>

#include <QStringList>

#include <Logger.h>

#include "couchdbproperties.h"
#include "couchdbresponsereceiver.h"
#include "couchdbrawresponsereceiver.h"

#include "couchdbdocumenthandler.h"
#include "couchdbattachmenthandler.h"
#include "couchdbviewhandler.h"
#include "couchdbfilterhandler.h"


const QString cdbqt::CouchDbClient::ATTACHMENT_FIELD_NAME = "_attachments";
const int cdbqt::CouchDbClient::HEART_BEAT = 30000;

cdbqt::CouchDbClient::CouchDbClient(cdbqt::CouchDbProperties *properties, QObject *parent)
    : CouchDbResponseReceiver(parent)
{
    initInstance(properties, parent);
}

void cdbqt::CouchDbClient::initInstance(cdbqt::CouchDbProperties *properties, QObject *parent)
{
    this->setParent(parent);
    this->properties = properties;
    networkManager = new QNetworkAccessManager(this);
    databaseInitalized = false;
    wasReinitChangesetAlreadyLogged = false;

    baseUrl.setScheme(properties->protocol());
    baseUrl.setHost(properties->host());
    baseUrl.setPort(properties->port());

    bool connected = connect(networkManager, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),
            this, SLOT(authenticationRequired(QNetworkReply*,QAuthenticator*)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbClient::init()
{
    createDatabase();
}

void cdbqt::CouchDbClient::startChangesetListening()
{
    if(databaseInitalized == false) {
        throw DatabaseWasNotInitializedException();
    }
    LOG_INFO() << "Starting changeset listener process";
    getDocumentById("", DATABASE_INFO_DOCUMENT);
}

void cdbqt::CouchDbClient::get(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariant &userData)
{
    if(databaseInitalized == false && responseReciever != this) {
        throw DatabaseWasNotInitializedException();
    }
    QNetworkReply *reply = networkManager->get(getNetworkRequest(url));
    ReplyHandler *handler = new ReplyHandler(userData, this);
    bool connected = connect(reply, SIGNAL(finished()), handler, SLOT(finished()));
    Q_ASSERT(connected == true);

    connected = connect(handler, SIGNAL(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)),
            responseReciever, SLOT(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbClient::get(const cdbqt::CouchDbRawResponseReceiver *rawResponseReciever, const QUrl &url, const QVariant &userData)
{
    if(databaseInitalized == false) {
        throw DatabaseWasNotInitializedException();
    }
    QNetworkReply *reply = networkManager->get(getNetworkRequest(url));
    RawReplyHandler *rawHandler = new RawReplyHandler(userData, this);
    bool connected = connect(reply, SIGNAL(finished()), rawHandler, SLOT(finished()));
    Q_ASSERT(connected == true);

    connected = connect(rawHandler, SIGNAL(responseDataReady(QByteArray,cdbqt::CouchDbError::Error,QVariant)),
            rawResponseReciever, SLOT(responseDataReady(QByteArray,cdbqt::CouchDbError::Error,QVariant)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbClient::put(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QByteArray &data, const QVariant &userData)
{
    if(databaseInitalized == false && responseReciever != this) {
        throw DatabaseWasNotInitializedException();
    }
    QNetworkReply *reply = networkManager->put(getNetworkRequest(url), data);
    ReplyHandler *handler = new ReplyHandler(userData, this);
    bool connected = connect(reply, SIGNAL(finished()), handler, SLOT(finished()));
    Q_ASSERT(connected == true);

    connected = connect(handler, SIGNAL(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)),
            responseReciever, SLOT(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbClient::put(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariantMap &jsonData, const QVariant &userData)
{
    if(databaseInitalized == false && responseReciever != this) {
        throw DatabaseWasNotInitializedException();
    }
    QJson::Serializer serializer;
    serializer.setDoublePrecision(10);
    bool serialized = false;
    QByteArray data = serializer.serialize(jsonData, &serialized);
    if(serialized == false) {
        LOG_ERROR() << "Could not serialize json data " << jsonData;
    }
    QNetworkReply *reply = networkManager->put(getNetworkRequest(url), data);
    ReplyHandler *handler = new ReplyHandler(userData, this);
    bool connected = connect(reply, SIGNAL(finished()), handler, SLOT(finished()));
    Q_ASSERT(connected == true);

    connected = connect(handler, SIGNAL(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)),
            responseReciever, SLOT(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbClient::deleteResource(const cdbqt::CouchDbResponseReceiver *responseReciever, const QUrl &url, const QVariant &userData)
{
    if(databaseInitalized == false && responseReciever != this) {
        throw DatabaseWasNotInitializedException();
    }
    QNetworkRequest request = getNetworkRequest(url);
    QNetworkReply *reply = networkManager->deleteResource(request);
    ReplyHandler *handler = new ReplyHandler(userData, this);
    bool connected = connect(reply, SIGNAL(finished()), handler, SLOT(finished()));
    Q_ASSERT(connected == true);

    connected = connect(handler, SIGNAL(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)),
            responseReciever, SLOT(responseDocumentReady(QVariantMap,cdbqt::CouchDbError::Error,QVariant)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbClient::initChangesetAfter()
{
    QUrl url = getUrlForPathForCurrentDatabase("_changes");
#if QT_VERSION >= 0x050000
    url.setQuery(QString("feed=continuous&heartbeat=%1&since=%2").arg(HEART_BEAT).arg(since));
#else
    url.setEncodedQuery(QString("feed=continuous&heartbeat=%1&since=%2").arg(HEART_BEAT).arg(since).toLocal8Bit());
#endif
    if(wasReinitChangesetAlreadyLogged == false) {
        LOG_INFO(QString("Listening database since %1").arg(since));
        wasReinitChangesetAlreadyLogged = true;
    }

    QNetworkReply *reply = networkManager->get(getNetworkRequest(url));
    connect(reply, SIGNAL(finished()), this, SLOT(changesetReadingFinished()));
    connect(reply, SIGNAL(readyRead()), this, SLOT(readChangeset()));
}

void cdbqt::CouchDbClient::responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData)
{
    if(!userData.isValid()) {
        LOG_ERROR() << "User data is not valid";
        return;
    }
    bool ok = false;
    RequestType requestType = (RequestType)userData.toInt(&ok);
    if(ok == false) {
        LOG_ERROR() << "Could not convert user data to request type";
        return;
    }
    switch (requestType) {
    case CREATING_DATABASE:
        if(error == cdbqt::CouchDbError::NO_ERROR) {
            LOG_INFO() << "Created new database";
            finishInitializationWithError(cdbqt::CouchDbError::NO_ERROR);
            return;
        } else if(error == cdbqt::CouchDbError::FILE_EXISTS) {
            LOG_INFO() << "Database already exist";
            finishInitializationWithError(cdbqt::CouchDbError::NO_ERROR);
            return;
        } else {
            LOG_WARNING() << "Failed to access database creation";
            finishInitializationWithError(error);
            return;
        }
        break;
    case DATABASE_INFO_DOCUMENT:
        if(error == cdbqt::CouchDbError::NO_ERROR) {
            since = document["update_seq"].toInt();
            LOG_INFO() << "Database info document recieved... Trying to initChangeset";
            connect(this, SIGNAL(initChangeset()), this, SLOT(initChangesetAfter()));
            emit(initChangeset());
        }
        break;
    case CHANGESET_DOCUMENT:
        wasReinitChangesetAlreadyLogged = false;
        if(error == CouchDbError::CONTENT_NOT_FOUND) {
            //document was deleted
        } else {
            emit(documentChanged(document, error));
        }
        break;
    }
}

cdbqt::CouchDbProperties *cdbqt::CouchDbClient::getProperties() const
{
    return properties;
}

QUrl cdbqt::CouchDbClient::getBaseUrl() const
{
    return baseUrl;
}

QUrl cdbqt::CouchDbClient::getUrlForPathForCurrentDatabase(const QString &path, bool encodeSlashes) const
{
    QUrl url = baseUrl;
    QByteArray result;
    result.append('/');
    result.append(QUrl::toPercentEncoding(properties->dbName()));
    if(path.size()) {
        if(encodeSlashes) {
            result.append('/');
            result.append(QUrl::toPercentEncoding(path));
        } else {
            QStringList list = path.split("/");
            foreach (QString string, list) {
                result.append('/');
                result.append(QUrl::toPercentEncoding(string));
            }
        }
    }
    url.setEncodedPath(result);

    return url;
}

QUrl cdbqt::CouchDbClient::getUrlForPathWithAttachmentForCurrentDatabase(const QString &path, const QString &attachment) const
{
    QUrl url = baseUrl;
    QByteArray result;
    result.append('/');
    result.append(QUrl::toPercentEncoding(properties->dbName()));
    if(path.size()) {
        result.append('/');
        result.append(QUrl::toPercentEncoding(path));
        result.append('/');
        result.append(QUrl::toPercentEncoding(attachment));
    }
    url.setEncodedPath(result);

    return url;
}

cdbqt::CouchDbDocumentHandler *cdbqt::CouchDbClient::createDocumentHandler(QObject *parent)
{
    return new CouchDbDocumentHandler(this, parent);
}

cdbqt::CouchDbAttachmentHandler *cdbqt::CouchDbClient::createAttachmentHandler(QObject *parent)
{
    return new CouchDbAttachmentHandler(this, parent);
}

cdbqt::CouchDbFilterHandler *cdbqt::CouchDbClient::createFilterHandler(const QString &filterPath, QObject *parent)
{
    return new CouchDbFilterHandler(filterPath, this, parent);
}

cdbqt::CouchDbFilterHandler *cdbqt::CouchDbClient::createFilterHandler(const QString &designDocumentId, const QString &filterName, const QString &filterJavascriptFunctionBody, QObject *parent)
{
    return new CouchDbFilterHandler(designDocumentId, filterName, filterJavascriptFunctionBody, this, parent);
}

cdbqt::CouchDbViewHandler *cdbqt::CouchDbClient::createViewHandler(const QString &viewPath, QObject *parent)
{
    return new CouchDbViewHandler(viewPath, this, parent);
}

cdbqt::CouchDbViewHandler *cdbqt::CouchDbClient::createViewHandler(const QString &designDocumentId, const QString &viewName, const QString &mapJavascriptFunctionBody, QObject *parent)
{
    return new CouchDbViewHandler(designDocumentId, viewName, mapJavascriptFunctionBody, this, parent);
}

void cdbqt::CouchDbClient::readChangeset()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    if(reply == NULL) {
        LOG_ERROR() << "Reply is NULL";
        return;
    }
    QJson::Parser parser;
    while(reply->canReadLine()) {
        QByteArray json = reply->readLine();
        if(json.trimmed().size() == 0) {
            continue;
        }
        bool ok = false;

        QVariant changesetVariant = parser.parse(json, &ok);

        if(ok == false) {
            LOG_ERROR(QString("Could not parse json for changeset: \"%1\" error: \"%2\"")
                            .arg(QString(json)).arg(parser.errorString()));

            continue;
        }
        QVariantMap changeset = changesetVariant.toMap();

        if(changeset.contains("seq")) {
            int seq = changeset["seq"].toInt();
            since = seq;
            QString id = changeset["id"].toString();
            getDocumentById(id, CHANGESET_DOCUMENT);
        } else if(changeset.contains("last_seq")) {
            int seq = changeset["last_seq"].toInt();
            since = seq;
            reply->abort();
        }
    }
}

void cdbqt::CouchDbClient::changesetReadingFinished()
{
    if(wasReinitChangesetAlreadyLogged == false) {
        LOG_WARNING(QString("Reinit changeset. Connection was lost"));
    }

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    if(reply != NULL) {
        reply->deleteLater();
    }
    emit(initChangeset());
}

void cdbqt::CouchDbClient::finishInitializationWithError(const cdbqt::CouchDbError::Error &error)
{
    if(error == cdbqt::CouchDbError::NO_ERROR) {
        databaseInitalized = true;
    }
    emit(couchDbClientFinishedInitialization(error));
}

void cdbqt::CouchDbClient::authenticationRequired(QNetworkReply *reply, QAuthenticator *auth)
{
    Q_UNUSED(reply);
    auth->setUser(properties->username());
    auth->setPassword(properties->password());
}

QNetworkRequest cdbqt::CouchDbClient::getNetworkRequest(const QUrl &url)
{
    QNetworkRequest request(url);
    QString concatenated = properties->username() + ":" + properties->password();
    QByteArray data = concatenated.toLocal8Bit().toBase64();
    QString headerData = "Basic " + data;
    request.setRawHeader("Authorization", headerData.toLocal8Bit());
    //for attachments
    request.setRawHeader("Accept", "application/json");

    return request;
}

void cdbqt::CouchDbClient::getDocumentById(const QString &id,
                                           const cdbqt::CouchDbClient::RequestType &requestType)
{
    QUrl url = getUrlForPathForCurrentDatabase(id);
    this->get(this, url, QVariant(requestType));
}

void cdbqt::CouchDbClient::createDatabase()
{
    LOG_INFO() << "creating database" + properties->dbName();
    QUrl url = getUrlForPathForCurrentDatabase("");
    QByteArray emptyData;

    this->put(this, url, emptyData, QVariant(CREATING_DATABASE));
}

cdbqt::ReplyHandler::ReplyHandler(const QVariant &userData, QObject *parent) :
    QObject(parent)
{
    this->userData = userData;
}

void cdbqt::ReplyHandler::finished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if(reply == NULL) {
        LOG_ERROR() << "reply is equal NULL";
        return;
    }
    QByteArray data = reply->readAll();
    QNetworkReply::NetworkError replyError = reply->error();
    QVariantMap document;
    cdbqt::CouchDbError::Error error = cdbqt::CouchDbError::NO_ERROR;
    QJson::Parser parser;
    if(replyError == QNetworkReply::NoError) {
        bool ok = false;
        QVariant documentVariant = parser.parse(data, &ok);
        if(ok) {
            document = documentVariant.toMap();
        } else {
            error = cdbqt::CouchDbError::COULD_NOT_PARSE_JSON;
        }
    } else {
        bool ok = false;
        QVariant documentVariant = parser.parse(data, &ok);
        QVariantMap errorDocument;
        if(ok) {
            errorDocument = documentVariant.toMap();
        }
        error = cdbqt::CouchDbError::networkReplyErrorToError(replyError, errorDocument);
    }
    reply->deleteLater();
    emit(responseDocumentReady(document, error, userData));
    this->deleteLater();
}


cdbqt::RawReplyHandler::RawReplyHandler(const QVariant &userData, QObject *parent) :
    QObject(parent)
{
    this->userData = userData;
}

void cdbqt::RawReplyHandler::finished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if(reply == NULL) {
        LOG_ERROR() << "reply is equal NULL";
        return;
    }
    QByteArray data = reply->readAll();
    QNetworkReply::NetworkError replyError = reply->error();
    cdbqt::CouchDbError::Error error = cdbqt::CouchDbError::NO_ERROR;
    if(replyError != QNetworkReply::NoError) {
        QVariantMap errorDocument;
        error = cdbqt::CouchDbError::networkReplyErrorToError(replyError, errorDocument);
        data.clear();
    }
    reply->deleteLater();
    emit(responseDataReady(data, error, userData));
    this->deleteLater();
}

