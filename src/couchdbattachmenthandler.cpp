#include "couchdbattachmenthandler.h"

#include <Logger.h>

#include "couchdbclient.h"

const QString cdbqt::CouchDbAttachmentHandler::EXTERNAL_USER_DATA = "externalUserData";
const QString cdbqt::CouchDbAttachmentHandler::INTERNAL_USER_DATA = "internalUserData";
const QString cdbqt::CouchDbAttachmentHandler::DOCUMENT_ID_DATA = "documentIdData";
const QString cdbqt::CouchDbAttachmentHandler::ATTACHMENT_PATH_DATA = "attachmentPathData";
const QString cdbqt::CouchDbAttachmentHandler::ATTACHMENT_REQUEST_GROUP_DATA = "attachmentRequestGroupData";

const QVariant cdbqt::CouchDbAttachmentHandler::SINGLE_ATTACHMENT_REQUEST = QVariant(0);
const QVariant cdbqt::CouchDbAttachmentHandler::ATTACHMENTS_REQUEST = QVariant(1);

cdbqt::CouchDbAttachmentHandler::CouchDbAttachmentHandler(cdbqt::CouchDbClient *client, QObject *parent) :
    CouchDbRawResponseReceiver(parent), client(client)
{
    bool connected = connect(this, SIGNAL(get(const cdbqt::CouchDbRawResponseReceiver*,QUrl,QVariant)),
                             client, SLOT(get(const cdbqt::CouchDbRawResponseReceiver*,QUrl,QVariant)));
    Q_ASSERT(connected == true);
}

void cdbqt::CouchDbAttachmentHandler::getAttachment(const QString &documentId, const QString &attachmentPath, const QVariant &userData)
{
    QUrl url = client->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentPath);
    QVariantMap userDataWrapper;
    userDataWrapper[INTERNAL_USER_DATA] = SINGLE_ATTACHMENT_REQUEST;
    userDataWrapper[EXTERNAL_USER_DATA] = userData;
    userDataWrapper[DOCUMENT_ID_DATA] = documentId;
    userDataWrapper[ATTACHMENT_PATH_DATA] = attachmentPath;

    emit(get(this, url, userDataWrapper));
}

void cdbqt::CouchDbAttachmentHandler::getAttachments(const QString &documentId, const QList<QString> &attachmentsPaths, const QVariant &userData)
{
    QVariantMap userDataWrapper;
    userDataWrapper[INTERNAL_USER_DATA] = ATTACHMENTS_REQUEST;
    userDataWrapper[EXTERNAL_USER_DATA] = userData;
    userDataWrapper[DOCUMENT_ID_DATA] = documentId;

    AttachmentRequestsGroup *attachmentRequestGroup = new AttachmentRequestsGroup(attachmentsPaths, this);
    QVariant variantPointer = qVariantFromValue((void *)attachmentRequestGroup);
    userDataWrapper[ATTACHMENT_REQUEST_GROUP_DATA] = variantPointer;

    foreach(QString attachmentPath, attachmentsPaths) {
        QUrl url = client->getUrlForPathWithAttachmentForCurrentDatabase(documentId, attachmentPath);
        userDataWrapper[ATTACHMENT_PATH_DATA] = attachmentPath;
        emit(get(this, url, userDataWrapper));
    }
}

void cdbqt::CouchDbAttachmentHandler::responseDataReady(const QByteArray &data, const cdbqt::CouchDbError::Error &error, const QVariant &userData)
{
    QVariantMap userDataWrapper = userData.toMap();
    if(userDataWrapper[INTERNAL_USER_DATA] == SINGLE_ATTACHMENT_REQUEST) {
        QString documentId = userDataWrapper[DOCUMENT_ID_DATA].toString();
        QString attachmentPath = userDataWrapper[ATTACHMENT_PATH_DATA].toString();

        emit(attachmentReady(documentId, attachmentPath, data, error, userDataWrapper[EXTERNAL_USER_DATA]));
    } else if(userDataWrapper[INTERNAL_USER_DATA] == ATTACHMENTS_REQUEST) {
        QString documentId = userDataWrapper[DOCUMENT_ID_DATA].toString();
        QString attachmentPath = userDataWrapper[ATTACHMENT_PATH_DATA].toString();
        QVariant variantPointer = userDataWrapper[ATTACHMENT_REQUEST_GROUP_DATA];
        AttachmentRequestsGroup *attachmentRequestGroup = (AttachmentRequestsGroup *)(variantPointer.value<void *>());
        if(attachmentRequestGroup == NULL) {
            LOG_WARNING() << "Attachment request group is null";
        } else {
            attachmentRequestGroup->addAttachment(attachmentPath, data, error);
            if(attachmentRequestGroup->allRequestsFinished()) {
                const QMap<QString, QByteArray> responseData = attachmentRequestGroup->getResponseData();
                const QVariant externalUserData = userDataWrapper[EXTERNAL_USER_DATA];

                emit(attachmentsReady(documentId, responseData, attachmentRequestGroup->getResponseErrors(), externalUserData));
                attachmentRequestGroup->deleteLater();
            }
        }
    }
}


cdbqt::AttachmentRequestsGroup::AttachmentRequestsGroup(const QList<QString> &attachmentsPaths, QObject *parent) :
        QObject(parent), attachmentsPaths(attachmentsPaths)
{
}

cdbqt::AttachmentRequestsGroup::AttachmentRequestsGroup(QObject *parent) :
        QObject(parent)
{
}

bool cdbqt::AttachmentRequestsGroup::allRequestsFinished()
{
    return attachmentsPaths.size() == responseData.size();
}

void cdbqt::AttachmentRequestsGroup::addAttachment(const QString &path, const QByteArray &data, const cdbqt::CouchDbError::Error &error)
{
    if(attachmentsPaths.contains(path)) {
        if(error != cdbqt::CouchDbError::NO_ERROR) {
            responseErrors[path] = error;
        }
        if(responseData.contains(path)) {
            LOG_WARNING() << QString("Replacing data for path '%1'").arg(path);
        }
        responseData[path] = data;
    } else {
        LOG_WARNING() << QString("Current request group doesn't contain attachment path '%1'").arg(path);
    }
}

QMap<QString, cdbqt::CouchDbError::Error> cdbqt::AttachmentRequestsGroup::getResponseErrors()
{
    return responseErrors;
}

QMap<QString, QByteArray> cdbqt::AttachmentRequestsGroup::getResponseData()
{
    return responseData;
}
