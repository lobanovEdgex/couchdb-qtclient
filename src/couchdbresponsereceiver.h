#ifndef COUCHDBRESPONSERECEIVER_H
#define COUCHDBRESPONSERECEIVER_H

#include <QObject>
#include <QVariant>

#include "couchdberror.h"

namespace cdbqt {

/** Class defines abstract slot for hanling document responses from couchdbClient
 */
class CouchDbResponseReceiver : public QObject
{
    Q_OBJECT
public:
    explicit CouchDbResponseReceiver(QObject *parent = 0);
public slots:
    virtual void responseDocumentReady(QVariantMap document, const cdbqt::CouchDbError::Error &error, const QVariant &userData) = 0;
};

}

#endif // COUCHDBHANDLER_H
