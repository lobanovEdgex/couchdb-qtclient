#include "couchdbutils.h"

#include <QString>
#include <QChar>
#include <QVariant>

const QString cdbqt::DOCUMENT_ID_FIELD = "_id";
const QString cdbqt::DOCUMENT_REVISION_FIELD = "_rev";
const QString cdbqt::ATTACHMENTS_FIELD = "_attachments";

bool cdbqt::isHexChar(const QChar &sym)
{
    QChar c = sym;
    if(c.isLetterOrNumber()) {
        c = c.toLower();
        if(c.isDigit()) {
            return true;
        }
        return c.unicode() >= 'a' && c.unicode() <= 'f';
    }
    return false;
}

QString cdbqt::id(const QVariantMap &document)
{
    return document[DOCUMENT_ID_FIELD].toString();
}

QVariantMap cdbqt::documentWithIdAndRevision(const QString &documentId, const QString &revision)
{
    QVariantMap document = documentWithId(documentId);
    if(revision.size() != 0) {
        document[DOCUMENT_REVISION_FIELD] = revision;
    }
    return document;
}

QVariantMap cdbqt::documentWithId(const QString &documentId)
{
    QVariantMap document;
    document[DOCUMENT_ID_FIELD] = documentId;

    return document;
}

QVariantMap cdbqt::getDocumentWithAttachment(const QVariantMap &document, const QString &attachmentName, const QByteArray &data)
{
    QVariantMap result = document;
    QVariantMap inlineAttachment;
    inlineAttachment["content_type"] = "text/plain";
    inlineAttachment["data"] = data.toBase64();
    if(!document.contains(ATTACHMENTS_FIELD)) {
        result[ATTACHMENTS_FIELD] = QVariantMap();
    }
    QVariantMap attachments = result[ATTACHMENTS_FIELD].toMap();
    attachments[attachmentName] = inlineAttachment;
    result[ATTACHMENTS_FIELD] = attachments;

    return result;
}

bool cdbqt::documentContainsAttachmentWithData(const QVariantMap &document, const QString &attachmentName)
{
    if(document.contains(ATTACHMENTS_FIELD)) {
        const QVariantMap &attachments = document[ATTACHMENTS_FIELD].toMap();
        if(attachments.contains(attachmentName)) {
            const QVariantMap &attachment = attachments[attachmentName].toMap();
            if(attachment.contains("data")) {
                return true;
            }
        }
    }
    return false;
}

QByteArray cdbqt::getDocumentAttachment(const QVariantMap &document, const QString &attachmentName)
{
    QByteArray result;
    if(documentContainsAttachmentWithData(document, attachmentName)) {
        result = QByteArray::fromBase64(document[ATTACHMENTS_FIELD].toMap()
                                        [attachmentName].toMap()["data"].toByteArray());
    }
    return result;
}
