#ifndef COUCHDBUTILS_H
#define COUCHDBUTILS_H

#include <QVariant>

class QString;
class QChar;

namespace cdbqt {
    const extern QString DOCUMENT_ID_FIELD;
    const extern QString DOCUMENT_REVISION_FIELD;
    const extern QString ATTACHMENTS_FIELD;
    QString id(const QVariantMap &document);
    QVariantMap documentWithIdAndRevision(const QString &documentId, const QString &revision);
    QVariantMap documentWithId(const QString &documentId);
    bool isHexChar(const QChar &c);
    QVariantMap getDocumentWithAttachment(const QVariantMap &document,
                                        const QString &attachmentName,
                                        const QByteArray &data);

    bool documentContainsAttachmentWithData(const QVariantMap &document, const QString &attachmentName);
    QByteArray getDocumentAttachment(const QVariantMap &document, const QString &attachmentName);

    class DatabaseWasNotInitializedException
    {
    };
}
#endif // COUCHDBDEFINITIONS_H
