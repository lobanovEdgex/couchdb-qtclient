#include "couchdbproperties.h"

cdbqt::CouchDbProperties::CouchDbProperties(const QString &dbName,
                                     QString protocol,
                                     QString host,
                                     int port,
                                     QString username,
                                     QString password,
                                     QObject *parent) :

    QObject(parent),
        m_dbName(dbName),
        m_protocol(protocol), m_host(host), m_port(port),
        m_username(username), m_password(password)
{

}

cdbqt::CouchDbProperties::CouchDbProperties(const QString &dbName,
                                            QObject *parent,
                                            QString protocol,
                                            QString host,
                                            int port,
                                            QString username,
                                            QString password) :
        QObject(parent),
        m_dbName(dbName),
        m_protocol(protocol), m_host(host), m_port(port),
        m_username(username), m_password(password)
{
}

QString cdbqt::CouchDbProperties::dbName() const
{
    return m_dbName;
}

QString cdbqt::CouchDbProperties::protocol() const
{
    return m_protocol;
}

QString cdbqt::CouchDbProperties::host() const
{
    return m_host;
}

int cdbqt::CouchDbProperties::port() const
{
    return m_port;
}

QString cdbqt::CouchDbProperties::username() const
{
    return m_username;
}

QString cdbqt::CouchDbProperties::password() const
{
    return m_password;
}
