#-------------------------------------------------
#
# Project created by QtCreator 2014-04-18T13:43:03
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = couchdb-qtclient
LIBS    += -lqjson -lLogger

SOURCES += \
    src/couchdbproperties.cpp \
    src/couchdbclient.cpp \
    src/couchdbresponsereceiver.cpp \
    src/couchdbdocumenthandler.cpp \
    src/couchdbutils.cpp \
    src/couchdbviewhandler.cpp \
    src/couchdberror.cpp \
    src/couchdbfilterhandler.cpp \
    src/couchdbrawresponsereceiver.cpp \
    src/couchdbattachmenthandler.cpp

HEADERS += \
    src/couchdbproperties.h \
    src/couchdbclient.h \
    src/couchdbresponsereceiver.h \
    src/couchdbdocumenthandler.h \
    src/couchdbutils.h \
    src/couchdbviewhandler.h \
    src/couchdberror.h \
    src/utils/cdbqtmetatyperegistration.h \
    src/couchdbfilterhandler.h \
    src/couchdbrawresponsereceiver.h \
    src/couchdbattachmenthandler.h

test {
    message(Test build)
    TEMPLATE = app

    QT += testlib
    TARGET = UnitTests

    SOURCES += tests/testcouchdbclient.cpp \
        tests/testutils.cpp \
        tests/testcouchdbdocumenthandler.cpp \
        tests/testcouchdbutils.cpp \
        tests/testcouchdbviewhandler.cpp \
	tests/testcouchdbfilterhandler.cpp \
        tests/testcouchdbattachmenthandler.cpp \
        tests/main.cpp

    HEADERS += tests/testcouchdbclient.h \
        tests/utils.h \
        tests/testutils.h \
        tests/testcouchdbdocumenthandler.h \
        tests/testcouchdbutils.h \
        tests/testcouchdbviewhandler.h \
        tests/testcouchdbattachmenthandler.h \
        tests/testcouchdbfilterhandler.h

} else {
    message(Normal build)
    TEMPLATE = lib
    CONFIG += staticlib

    unix:!symbian {
        maemo5 {
            target.path = /opt/usr/lib
        } else {
            target.path = /usr/lib
        }
        INSTALLS += target
    }
}
